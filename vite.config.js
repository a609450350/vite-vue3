import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import AutoImport from "unplugin-auto-import/vite";
import Components from "unplugin-vue-components/vite";
import IconsResolver from "unplugin-icons/resolver";
import Icons from "unplugin-icons/vite";
import { ElementPlusResolver } from "unplugin-vue-components/resolvers";
import WindiCSS from "vite-plugin-windicss";
import viteCompression from "vite-plugin-compression";
import svgLoader from "vite-svg-loader";
import { printLogo } from "./src/utils/copyright.js";
// https://vitejs.dev/config/

export default defineConfig({
  plugins: [
    vue(),
    svgLoader(),
    WindiCSS(),
    // viteCompression(),
    AutoImport({
      resolvers: [
        ElementPlusResolver({ importStyle: "sass" }),
        IconsResolver({
          prefix: "Icon",
        }),
      ],
      imports: ["vue", "vue-router", "pinia", "vue-router"],
      dts: "src/auto-imports.d.ts",
    }),
    Components({
      dirs: ["src/components", "src/layout/default", "src/layout/fi"],
      deep: true, // auto import sub components
      extensions: ["vue"],
      resolvers: [
        ElementPlusResolver({ importStyle: "sass" }),
        IconsResolver(),
      ],
    }),
    // 开发环境完整引入element - plus,
    // {
    //   name: "dev-auto-import-element-plus",
    //   transform(code, id) {
    //     const name = "ElementPlus";
    //     // 引入 ElementPlus 和 样式
    //     const prepend = `import ${name} from 'element-plus';\nimport 'element-plus/dist/index.css';\nimport '@/style/element-ui/color.scss';\n`;

    //     if (process.env.NODE_ENV !== "production" && /src\/main.js$/.test(id)) {
    //       code = code.replace(".mount(", ($1) => `.use(${name})${$1}`);
    //       return prepend + code;
    //     }
    //   },
    // },
    Icons({
      compiler: "vue3",
      autoInstall: true,
    }),
  ],
  resolve: {
    alias: {
      "@": "/src",
    },
  },
  // optimizeDeps: {
  //   include: [
  //     "@amap/amap-jsapi-loader",
  //     "@antv/x6",
  //     "@vicons/antd",
  //     "@vueuse/core",
  //     "axios",
  //     "crypto-js",
  //     "dayjs",
  //     "echarts",
  //     "element-plus",
  //     "element-plus/es",
  //     "element-plus/es/components/alert/style/index",
  //     "element-plus/es/components/aside/style/index",
  //     "element-plus/es/components/avatar/style/index",
  //     "element-plus/es/components/backtop/style/index",
  //     "element-plus/es/components/breadcrumb-item/style/index",
  //     "element-plus/es/components/breadcrumb/style/index",
  //     "element-plus/es/components/button/style/index",
  //     "element-plus/es/components/card/style/index",
  //     "element-plus/es/components/cascader/style/index",
  //     "element-plus/es/components/checkbox/style/index",
  //     "element-plus/es/components/color-picker/style/index",
  //     "element-plus/es/components/container/style/index",
  //     "element-plus/es/components/date-picker/style/index",
  //     "element-plus/es/components/descriptions-item/style/index",
  //     "element-plus/es/components/descriptions/style/index",
  //     "element-plus/es/components/dialog/style/index",
  //     "element-plus/es/components/divider/style/index",
  //     "element-plus/es/components/drawer/style/index",
  //     "element-plus/es/components/dropdown-item/style/index",
  //     "element-plus/es/components/dropdown-menu/style/index",
  //     "element-plus/es/components/dropdown/style/index",
  //     "element-plus/es/components/empty/style/index",
  //     "element-plus/es/components/form-item/style/index",
  //     "element-plus/es/components/form/style/index",
  //     "element-plus/es/components/header/style/index",
  //     "element-plus/es/components/icon/style/index",
  //     "element-plus/es/components/image/style/index",
  //     "element-plus/es/components/input-number/style/index",
  //     "element-plus/es/components/input/style/index",
  //     "element-plus/es/components/link/style/index",
  //     "element-plus/es/components/loading/style/index",
  //     "element-plus/es/components/main/style/index",
  //     "element-plus/es/components/menu-item/style/index",
  //     "element-plus/es/components/menu/style/index",
  //     "element-plus/es/components/message/style/index",
  //     "element-plus/es/components/option/style/index",
  //     "element-plus/es/components/page-header/style/index",
  //     "element-plus/es/components/pagination/style/index",
  //     "element-plus/es/components/popconfirm/style/index",
  //     "element-plus/es/components/radio-group/style/index",
  //     "element-plus/es/components/radio/style/index",
  //     "element-plus/es/components/scrollbar/style/index",
  //     "element-plus/es/components/select-v2/style/index",
  //     "element-plus/es/components/select/style/index",
  //     "element-plus/es/components/skeleton/style/index",
  //     "element-plus/es/components/sub-menu/style/index",
  //     "element-plus/es/components/switch/style/index",
  //     "element-plus/es/components/tab-pane/style/index",
  //     "element-plus/es/components/table-column/style/index",
  //     "element-plus/es/components/table/style/index",
  //     "element-plus/es/components/tabs/style/index",
  //     "element-plus/es/components/tag/style/index",
  //     "element-plus/es/components/timeline-item/style/index",
  //     "element-plus/es/components/timeline/style/index",
  //     "element-plus/es/components/transfer/style/index",
  //     "element-plus/es/components/tree/style/index",
  //     "element-plus/es/components/upload/style/index",
  //     "element-plus/es/components/button-group/style/index",
  //     "element-plus/es/components/row/style/index",
  //     "element-plus/es/components/col/style/index",
  //     "element-plus/lib/locale/lang/en",
  //     "element-plus/lib/locale/lang/zh-cn",
  //     "js-cookie",
  //     "lodash-es",
  //     "nprogress",
  //     "path-browserify",
  //     "pinia",
  //     "query-string",
  //     "screenfull",
  //     "sortablejs",
  //     "vue",
  //     "vue-router",
  //   ],
  // },
});
