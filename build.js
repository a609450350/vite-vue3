import { readFileSync, writeFileSync } from "fs";
import dayjs from "dayjs";

console.log("====================构建开始====================");

const file = "./src/utils/copyright.js";
const timeExp = /buildDate.*=.*'(.*)';/;

let copyrightFile = readFileSync(file, "utf-8");
const buildDate = dayjs().format("YYYY-MM-DD HH:mm:ss");
writeFileSync("./build.time", buildDate);
console.log(`构建时间:${buildDate}`);
copyrightFile = copyrightFile.replace(
  timeExp.exec(copyrightFile)[1],
  buildDate
);
writeFileSync(file, copyrightFile);

const versionFile = "./public/static/json/version.json";
writeFileSync(
  versionFile,
  JSON.stringify({
    buildTime: Date.now(),
  })
);
