import { readFileSync, unlinkSync } from "fs";
import dayjs from "dayjs";

console.log("====================构建完成====================");
const finishDate = dayjs().format("YYYY-MM-DD HH:mm:ss");
const startDate = readFileSync("./build.time", "utf-8");
unlinkSync("./build.time");
console.log("完成时间", finishDate);
console.log(
  "构建耗时",
  dayjs(finishDate).diff(dayjs(startDate), "second"),
  "s"
);

console.log("====================构建完成====================");
