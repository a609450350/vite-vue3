import { createApp } from "vue";
import { setupStore } from "@/store";
import { setupElIcon } from "@/utils/el-icon-helper";
import "element-plus/theme-chalk/dark/css-vars.css";
import "./style/element-ui/color.scss";
import "./style/index.scss";
// import ElementPlus from "element-plus";
// import "element-plus/dist/index.css";
// import "./style.css";

import "virtual:windi.css";
import "@/router/guard";
import App from "./App.vue";
import router from "./router/index";

const app = createApp(App);
app.use(router);
setupStore(app);
setupElIcon(app);
app.mount("#app");
