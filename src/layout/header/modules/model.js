export default function useModel() {
  const model = () => ({
    name: '',
    nickname: '',
    sex: '',
    age: '',
    email: '',
    mobile: '',
    cardId: '',
    companyIds: '',
    extFields: '',
    leaderId: '',
    enabled: '',
    accountImg: '',
  });
  const rules = reactive({
    name: [{ required: true, message: '请输入姓名', trigger: 'change' }],
    nickname: [{ required: true, message: '请输入昵称', trigger: 'change' }],
    sex: [{ required: true, message: '请输入性别', trigger: 'change' }],
    age: [{ required: true, message: '请输入年龄', trigger: 'change' }],
    email: [{ required: true, message: '请输入邮箱', trigger: 'change' }],
    mobile: [{ required: true, message: '请输入手机号', trigger: 'change' }],
    cardId: [{ required: true, message: '请输入身份证号', trigger: 'change' }],
    companyIds: [{ required: true, message: '请输入所拥有的数据权限', trigger: 'change' }],
    extFields: [{ required: true, message: '请输入扩展信息', trigger: 'change' }],
    leaderId: [{ required: true, message: '请输入直接上级', trigger: 'change' }],
    enabled: [{ required: true, message: '请输入账号状态', trigger: 'change' }],
    accountImg: [{ required: true, message: '请输入用户头像', trigger: 'change' }],
  });
  return { model, rules };
}
