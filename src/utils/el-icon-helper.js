import {
  AndroidFilled,
  ApartmentOutlined,
  ApiTwotone,
  AppstoreOutlined,
  BellTwotone,
  BookTwotone,
  BranchesOutlined,
  BugTwotone,
  CalculatorTwotone,
  CarryOutTwotone,
  CloudTwotone,
  ControlTwotone,
  CrownTwotone,
  DashboardOutlined,
  DashboardTwotone,
  DatabaseTwotone,
  DeploymentUnitOutlined,
  EnvironmentTwotone,
  FileTextTwotone,
  FolderTwotone,
  GlobalOutlined,
  HddTwotone,
  InteractionTwotone,
  LockTwotone,
  MoneyCollectTwotone,
  PictureTwotone,
  PrinterTwotone,
  ProjectTwotone,
  RedditOutlined,
  SafetyCertificateTwotone,
  SearchOutlined,
  SettingTwotone,
  ShoppingOutlined,
  SolutionOutlined,
  ToolTwotone,
} from '@vicons/antd';

const antdUsedIcons = {
  'Antd-AndroidFilled': AndroidFilled,
  'Antd-ApartmentOutlined': ApartmentOutlined,
  'Antd-ApiTwotone': ApiTwotone,
  'Antd-AppstoreOutlined': AppstoreOutlined,
  'Antd-BellTwotone': BellTwotone,
  'Antd-BookTwotone': BookTwotone,
  'Antd-BranchesOutlined': BranchesOutlined,
  'Antd-BugTwotone': BugTwotone,
  'Antd-CalculatorTwotone': CalculatorTwotone,
  'Antd-CarryOutTwotone': CarryOutTwotone,
  'Antd-CloudTwotone': CloudTwotone,
  'Antd-ControlTwotone': ControlTwotone,
  'Antd-CrownTwotone': CrownTwotone,
  'Antd-DashboardOutlined': DashboardOutlined,
  'Antd-DashboardTwotone': DashboardTwotone,
  'Antd-DatabaseTwotone': DatabaseTwotone,
  'Antd-DeploymentUnitOutlined': DeploymentUnitOutlined,
  'Antd-EnvironmentTwotone': EnvironmentTwotone,
  'Antd-FileTextTwotone': FileTextTwotone,
  'Antd-FolderTwotone': FolderTwotone,
  'Antd-GlobalOutlined': GlobalOutlined,
  'Antd-HddTwotone': HddTwotone,
  'Antd-InteractionTwotone': InteractionTwotone,
  'Antd-LockTwotone': LockTwotone,
  'Antd-MoneyCollectTwotone': MoneyCollectTwotone,
  'Antd-PictureTwotone': PictureTwotone,
  'Antd-PrinterTwotone': PrinterTwotone,
  'Antd-ProjectTwotone': ProjectTwotone,
  'Antd-RedditOutlined': RedditOutlined,
  'Antd-SafetyCertificateTwotone': SafetyCertificateTwotone,
  'Antd-SearchOutlined': SearchOutlined,
  'Antd-SettingTwotone': SettingTwotone,
  'Antd-ShoppingOutlined': ShoppingOutlined,
  'Antd-SolutionOutlined': SolutionOutlined,
  'Antd-ToolTwotone': ToolTwotone,
};

export function setupElIcon(app) {
  Object.keys(antdUsedIcons).forEach((item) => {
    app.component(`${item}`, antdUsedIcons[item]);
  });
}
