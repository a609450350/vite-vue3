import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function approvalInfoList(page, limit, query) {
  return httpGet('biz/approvalInfo/page', {
    page,
    limit,
    ...query,
  });
}

export function approvalInfoAdd(form) {
  return httpPost('biz/approvalInfo/addApprovalInfo', form);
}

export function approvalInfoEdit(form) {
  return httpPut('biz/approvalInfo', form);
}

export function approvalInfoDelete(id) {
  return httpDelete(`biz/approvalInfo/${id}`);
}

export function repealApprovalInfo(form) {
  return httpPost('biz/approvalInfo/repealApprovalInfo', form);
}

export function submitApprovalInfo(form) {
  return httpPost('biz/approvalInfo/submitApprovalInfo', form);
}
