import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function getIcCardMeterList(page, limit, query) {
  return httpGet('biz/business/getIcCardMeterList', {
    page,
    limit,
    ...query,
  });
}

// 读卡
export function addIcReadCard(form) {
  return httpPost('biz/business/addIcReadCard', form);
}

// 制卡
export function addIcMakeCard(form) {
  return httpPost('biz/business/addIcMakeCard', form);
}

// 补卡
export function addIcRepairCard(form) {
  return httpPost('biz/business/addIcRepairCard', form);
}

// 补量
export function addIcRepairGas(form) {
  return httpPost('biz/business/addIcRepairGas', form);
}

// 回收
export function addIcRecycle(form) {
  return httpPost('biz/business/addIcRecycle', form);
}

// 清零
export function addIcZeroClearing(form) {
  return httpPost('biz/business/addIcZeroClearing', form);
}

// 充值
export function addIcMeterPayFee(form) {
  return httpPost('biz/custPayInfo/addIcMeterPayFee', form);
}

// 退补
export function addIcBackFill(form) {
  return httpPost('biz/business/addIcBackFill', form);
}

// 取消退补
export function addIcCancelBackFill(form) {
  return httpPost('biz/business/addIcCancelBackFill', form);
}

// 换表补量
export function addIcUpMeterRepairGas(form) {
  return httpPost('biz/business/addIcUpMeterRepairGas', form);
}

// 本地服务结果上传
export function upMeterOperLog(form) {
  return httpPost('biz/business/upMeterOperLog', form);
}

// 操作记录
export function getCardOptLogList(page, limit, query) {
  return httpGet('biz/business/getCardOptLogList', {
    page,
    limit,
    ...query,
  });
}

export function getPayDataList(page, limit, name) {
  return httpGet('biz/custPayInfo/getPayICMeterDataList', {
    page,
    limit,
    name,
  });
}

export function getAmountPayable(form) {
  return httpGet('biz/custPayInfo/getAmountPayable', form);
}
