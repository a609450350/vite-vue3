import { httpDelete, httpGet, httpPost, httpPostForm, httpPut } from '@/api/index';

export function addrBuildingList(page, limit, query, name) {
  return httpGet('biz/business/getBatchOpenAcctList', {
    page,
    limit,
    ...query,
    name,
  });
}

export function getBatchOpenAcctExportTemp(form) {
  return httpGet('biz/business/getBatchOpenAcctExportTemp', form);
}

export function addBatchOpenAcctImportTemp(form) {
  return httpPostForm('biz/business/addBatchOpenAcctImportTemp', form);
}

export function addBatchOpenAcct(form) {
  return httpPost('biz/business/addBatchOpenAcct', form);
}

// 批量开通
export function getBatchFireList(page, limit, query, name) {
  return httpGet('biz/business/getBatchFireList', {
    page,
    limit,
    ...query,
    name,
  });
}

export function getBatchFireExportTemp(form) {
  return httpGet('biz/business/getBatchFireExportTemp', form);
}

export function addBatchFireImportTemp(form) {
  return httpPostForm('biz/business/addBatchFireImportTemp', form);
}

export function addBatchFire(form) {
  return httpPost('biz/business/addBatchFire', form);
}

// 批量装表
export function getBatchInstallMeterList(page, limit, query, name) {
  return httpGet('biz/business/getBatchInstallMeterList', {
    page,
    limit,
    ...query,
    name,
  });
}

export function getBatchInsMeterExportTemp(form) {
  return httpGet('biz/business/getBatchInsMeterExportTemp', form);
}

export function addBatchInsMeterImportTemp(form) {
  return httpPostForm('biz/business/addBatchInsMeterImportTemp', form);
}

export function addBatchInsMeter(form) {
  return httpPost('biz/business/addBatchInsMeter', form);
}

// 批量换表
export function getBatchMeterChangeList(page, limit, query, name) {
  return httpGet('biz/business/getBatchMeterChangeList', {
    page,
    limit,
    ...query,
    name,
  });
}

export function getBatchMeterChangeExportTemp(form) {
  return httpGet('biz/business/getBatchMeterChangeExportTemp', form);
}

export function addBatchMeterChangeImportTemp(form) {
  return httpPostForm('biz/business/addBatchMeterChangeImportTemp', form);
}

export function addBatchMeterChange(form) {
  return httpPost('biz/business/addBatchMeterChange', form);
}

// 批量变价
export function getBatchPriceAdjustmentList(page, limit, query) {
  return httpGet('biz/business/getBatchPriceAdjustmentList', {
    page,
    limit,
    ...query,
  });
}

export function addBatchPriceAdjustment(form) {
  return httpPost('biz/business/addBatchPriceAdjustment', form);
}
