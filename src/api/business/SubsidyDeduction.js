import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function subsidyDeductionList(page, limit, query) {
  return httpGet('biz/sysConfSubsidyLog/page', {
    page,
    limit,
    ...query,
  });
}

export function updateExpenseDeductible(form) {
  return httpPost('biz/sysConfSubsidyLog/updateExpenseDeductible', form);
}

export function updateInvalidSubsidy(form) {
  return httpPost('biz/sysConfSubsidyLog/updateInvalidSubsidy', form);
}
