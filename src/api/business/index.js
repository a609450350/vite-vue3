import { httpGet, httpPost } from '@/api/index';

export function serviceList(page, limit, query, name) {
  return httpGet('biz/business/page', {
    page,
    limit,
    ...query,
    name,
  });
}

export function getBusiDataByAddrId(id) {
  return httpGet('biz/business/getBusiDataByAddrId', {
    id,
  });
}

export function addOpenAccount(form) {
  return httpPost('biz/business/addOpenAccount', form);
}
export function addCustInfoUpdate(form) {
  return httpPost(`biz/business/addCustInfoUpdate`, form);
}

export function addTransferOwnership(form) {
  return httpPost('biz/business/addTransferOwnership', form);
}

export function addInstallMeter(form) {
  return httpPost('biz/business/addInstallMeter', form);
}

export function addMeterChange(form) {
  return httpPost('biz/business/addMeterChange', form);
}

export function addOpenFire(form) {
  return httpPost('biz/business/addOpenFire', form);
}

export function addMeterStop(form) {
  return httpPost('biz/business/addMeterStop', form);
}

export function addMeterEnable(form) {
  return httpPost('biz/business/addMeterEnable', form);
}

export function addMeterDismantle(form) {
  return httpPost('biz/business/addMeterDismantle', form);
}

export function addAddrDismantle(form) {
  return httpPost('biz/business/addAddrDismantle', form);
}

export function addAddrCancellation(form) {
  return httpPost('biz/business/addAddrCancellation', form);
}

export function addMeterModelChange(form) {
  return httpPost('biz/business/addMeterModelChange', form);
}

export function addPriceAdjustment(form) {
  return httpPost('biz/business/addPriceAdjustment', form);
}

export function updateCarSealCode(form) {
  return httpPost('biz/business/updateCarSealCode', form);
}

export function getBusiByAddrId(id) {
  return httpGet(`biz/addrInfo/${id}`);
}
