import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

// 获取列表
export function sysConfSubsidyAct(page, limit, query) {
  return httpGet('biz/sysConfSubsidyAct/page', {
    page,
    limit,
    ...query,
  });
}
// 新增查询
export function selectSynthesis(name) {
  return httpGet('biz/custIdentity/selectSynthesis', {
    name,
  });
}
// 账户流水查询
export function selectActFlow(subsidyActId, page, limit, query) {
  return httpGet('biz/sysConfSubsidyAct/selectActFlow', {
    subsidyActId,
    page,
    limit,
    ...query,
  });
}
// 发放补贴
export function issueOffers(subsidyActId, query) {
  return httpPost('biz/sysConfSubsidyAct/issueOffers', {
    subsidyActId,
    ...query,
  });
}
// 配置列表
export function selectListByParam(subsidyQtyType) {
  return httpGet('biz/sysConfSubsidy/selectListByParam', { subsidyQtyType });
}
// 新增优惠
export function addActMapper(confActId, confId, startTime, endTime) {
  return httpPost('biz/sysConfSubsidyAct/addActMapper', { confId, confActId, startTime, endTime });
}
// 新增优惠账户
export function addSysConfSubsidyAct(actId, subsidyQtyType) {
  return httpPost('biz/sysConfSubsidyAct/addSysConfSubsidyAct', { actId, subsidyQtyType });
}
// 删除账户
export function deleteSysConfSubsidyAct(id) {
  return httpDelete(`biz/sysConfSubsidyAct/${id}`);
}
