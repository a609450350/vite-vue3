import { httpGet, httpPost, httpPut } from './index';

export function login(data) {
  return httpPost('auth/jwt/token', data);
}

export function fiLogin() {
  return httpPost('auth/jwt/emstoken', {});
}

export function mpsFunc(accessToken) {
  return httpGet(
    '/mps/console/prod-api/resource/login/user',
    {
      access_token: accessToken,
    },
    false
  );
}

export function mpsUser(accessToken) {
  return httpGet(
    '/mps/system/prod-api/system/user/profile',
    {
      access_token: accessToken,
    },
    false
  );
}

export function accessKey() {
  return httpGet('auth/jwt/userKey');
}

export function getInfo() {
  return httpGet('admin/account/getUser');
}

export function getElements() {
  return httpGet('admin/element/user/menu');
}

export function getUserMenu() {
  return httpGet('admin/menu/user/authorityTree');
}

export function dictionaryList() {
  // 获取字典列表
  const env = import.meta.env.VITE_APP_ENV;
  const localDic = localStorage.getItem('dicDatabase');
  if (env === 'test' && localDic) {
    return new Promise((resolve) => {
      resolve(JSON.parse(localDic));
    });
  }

  return httpGet('admin/dictionary/getDictionaryList', { industryCode: '1001' });
}

// 获取验证码
export function getPwdCode(key) {
  return httpGet(`auth/jwt/forgetPwd/addSendMsg/${key}`);
}

// 忘记密码修改密码
export function updatePwd(data) {
  return httpPost('auth/jwt/forgetPwd/updatePwd', data);
}

// 验证码登录
export function phoneLogin(data) {
  return httpPost('auth/jwt/validateByCode', data);
}

// 修改密码
export function changePwd(data) {
  return httpPut('admin/account/changePwd', data);
}

export function getCompanyInfoByUser() {
  return httpGet(`biz/stationCompany/getCompanyInfoByUser`);
}
