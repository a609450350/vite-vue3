import { httpGet, httpPost } from '@/api/index';

export function getUserByCompanyId() {
  return httpGet('biz/busUser/getUserByCompanyId');
}
export function getSysConfByKey(key) {
  return httpGet('biz/sysConfData/getSysConfByKey', { key });
}

export function gbCityCode(parentId = '') {
  return httpGet('admin/gbCityCode/page', { parentId });
}
export function getGbAddrCode(key, name) {
  return httpGet('biz/gbCityCode/getGbAddrCode', { key, name });
}

export function gbIndustryCode(pid = '') {
  return httpGet('admin/baseIndustry/getBaseIndustryByPid', { pid });
}

export function stationInfoAll(areaId) {
  return httpGet('biz/stationInfo/page', { page: 1, limit: 9999, areaId });
}

export function getCalc(data) {
  return httpPost('biz/custPayInfo/getCalculateRul', { data });
}

export function getMeterModelTree() {
  return httpGet('biz/meterModel/getMeterModelTree');
}
