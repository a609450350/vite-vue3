import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function wechatAliPay(form) {
  return httpPost('biz/custPayInfo/addWxZfb', form);
}

export function cancelWechatAliPay(form) {
  return httpPost('biz/custPayInfo/addRefundWxZfb', form);
}
