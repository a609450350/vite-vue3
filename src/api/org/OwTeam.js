import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function owTeamList(page, limit, query) {
  return httpGet('zoyoact/moveTeam/page', {
    page,
    limit,
    ...query,
  });
}

export function owTeamAdd(form) {
  return httpPost('zoyoact/moveTeam', form);
}

export function owTeamEdit(form) {
  return httpPut('zoyoact/moveTeam', form);
}

export function owTeamDelete(id) {
  return httpDelete(`zoyoact/moveTeam/${id}`);
}

export function owTeamBindUser(form) {
  return httpPost(`zoyoact/moveTeam/addTeamUser`, form);
}

export function owTeamUser(teamId) {
  return httpGet(`zoyoact/moveTeam/getTeamUserList`, { teamId });
}
