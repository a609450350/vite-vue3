import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function stationInfoList(page, limit, query) {
  return httpGet('biz/stationInfo/page', {
    page,
    limit,
    ...query,
  });
}

export function stationInfoAdd(form) {
  return httpPost('biz/stationInfo', form);
}

export function stationInfoEdit(form) {
  return httpPut('biz/stationInfo', form);
}

export function stationInfoDelete(id) {
  return httpDelete(`biz/stationInfo/${id}`);
}
