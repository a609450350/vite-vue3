import { httpGet, httpPost } from '@/api/index';

export function busUserList(page, limit, query) {
  return httpGet('biz/busUser/page', {
    page,
    limit,
    ...query,
  });
}

export function busUserAdd(form) {
  return httpPost('biz/busUser/add', form);
}

export function busUserEdit(form) {
  return httpPost('biz/busUser/update', form);
}

export function busUserDelete(id) {
  return httpPost(`biz/busUser/${id}`);
}

export function busUserInfo(id) {
  return httpGet(`biz/busUser/${id}`);
}

export function getUserListAll() {
  return httpGet(`biz/busUser/getUserListAll`);
}
