import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function mrTeamList(page, limit, query) {
  return httpGet('biz/mrTeam/page', {
    page,
    limit,
    ...query,
  });
}

export function mrTeamAdd(form) {
  return httpPost('biz/mrTeam', form);
}

export function mrTeamEdit(form) {
  return httpPut('biz/mrTeam', form);
}

export function mrTeamDelete(id) {
  return httpDelete(`biz/mrTeam/${id}`);
}

export function mrTeamBindUser(form) {
  return httpPost(`biz/mrTeam/addTeamUser`, form);
}

export function mrTeamUser(teamId) {
  return httpGet(`biz/mrTeam/getTeamUserList`, { teamId });
}
