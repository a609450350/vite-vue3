import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

import { useAppStoreWithOut } from '@/store/modules/app';

export function stationCompanyTree() {
  const appStore = useAppStoreWithOut();
  const { appKey } = appStore;
  return httpGet(`admin/organization/getOrgTree?appKey=${appKey}`);
}

export function stationCompanyList(page, limit, query) {
  return httpGet('biz/stationCompany/page', {
    page,
    limit,
    ...query,
  });
}

export function stationCompanyAdd(form) {
  return httpPost('biz/stationCompany', form);
}

export function stationCompanyEdit(form) {
  return httpPut('biz/stationCompany', form);
}

export function stationCompanyDelete(id) {
  return httpDelete(`biz/stationCompany/${id}`);
}
