import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function scTeamList(page, limit, query) {
  return httpGet('sc/scTeam/page', {
    page,
    limit,
    ...query,
  });
}

export function scTeamAdd(form) {
  return httpPost('sc/scTeam', form);
}

export function scTeamEdit(form) {
  return httpPut('sc/scTeam', form);
}

export function scTeamDelete(id) {
  return httpDelete(`sc/scTeam/${id}`);
}

export function scTeamUser(teamId) {
  return httpGet(`sc/scTeam/getTeamUserList`, { teamId });
}

export function scTeamBindUser(form) {
  return httpPost(`sc/scTeam/addTeamUser`, form);
}

export function orgAllUserList() {
  return httpGet('admin/account/getUserListByCompanyId', {});
}
