import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function baseUserList(page, limit, query) {
  return httpGet('admin/account/page', {
    page,
    limit,
    ...query,
  });
}

export function baseUserAdd(form) {
  return httpPost('admin/account', form);
}

export function baseUserEdit(form) {
  return httpPut('admin/account', form);
}

export function baseUserDelete(id) {
  return httpDelete(`admin/account/${id}`);
}

export function userPwdReset(username) {
  return httpPost('admin/account/resetPwd', { username });
}
