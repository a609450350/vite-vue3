import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function organizationTree() {
  return httpGet('admin/organization/getOrgTree', {
    appKey: `${import.meta.env.VITE_APP_KEY}`,
  });
}

export function organizationAdd(form) {
  return httpPost('admin/organization', form);
}

export function organizationEdit(form) {
  return httpPut('admin/organization', form);
}

export function organizationDelete(id) {
  return httpDelete(`admin/organization/${id}`);
}
