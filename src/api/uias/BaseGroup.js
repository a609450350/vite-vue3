import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function baseGroupTree(parentId = -1) {
  return httpGet('admin/group/list', {
    appKey: `${import.meta.env.VITE_APP_KEY}`,
    parentId,
  });
}

export function baseGroupType() {
  return httpGet('admin/groupType/all');
}

export function baseGroupAdd(form) {
  return httpPost('admin/group', { ...form, appKey: `${import.meta.env.VITE_APP_KEY}`, tendId: 1 });
}

export function baseGroupEdit(form) {
  return httpPut('admin/group', { ...form, appKey: `${import.meta.env.VITE_APP_KEY}`, tendId: 1 });
}

export function baseGroupDelete(id) {
  return httpDelete(`admin/group/${id}`);
}

export function baseMenuAll() {
  return httpGet('admin/menu/getEleTreeUser', { appKey: `${import.meta.env.VITE_APP_KEY}` });
}

export function grantedMenuList(id) {
  return httpGet(`admin/group/authority/menu/${id}`);
}
