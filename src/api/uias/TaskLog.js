import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function taskLogList(page, limit, query) {
  return httpGet('job/taskLog/page', {
    page,
    limit,
    ...query,
  });
}

export function taskLogAdd(form) {
  return httpPost('job/taskLog', form);
}

export function taskLogEdit(form) {
  return httpPut('job/taskLog', form);
}

export function taskLogDelete(id) {
  return httpDelete(`job/taskLog/${id}`);
}
