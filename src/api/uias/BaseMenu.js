import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function baseMenuTree() {
  return httpGet('admin/menu/tree', {
    appKey: `${import.meta.env.VITE_APP_KEY}`,
  });
}

export function baseMenuAdd(form) {
  return httpPost('admin/menu', { ...form, appKey: `${import.meta.env.VITE_APP_KEY}` });
}

export function baseMenuEdit(form) {
  return httpPut('admin/menu', { ...form, appKey: `${import.meta.env.VITE_APP_KEY}` });
}

export function baseMenuDelete(id) {
  return httpDelete(`admin/menu/${id}`);
}
