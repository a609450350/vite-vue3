import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function baseDictionaryTree() {
  return httpGet('admin/dictionary/getDictionaryTree', {
    effectScope: `${import.meta.env.VITE_APP_KEY}`,
  });
}

export function baseDictionaryAdd(form) {
  return httpPost('admin/dictionary', { ...form, effectScope: `${import.meta.env.VITE_APP_KEY}` });
}

export function baseDictionaryEdit(form) {
  return httpPut('admin/dictionary', { ...form, effectScope: `${import.meta.env.VITE_APP_KEY}` });
}

export function baseDictionaryDelete(id) {
  return httpDelete(`admin/dictionary/${id}`);
}
