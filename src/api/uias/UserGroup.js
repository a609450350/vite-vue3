import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function userGroupList(page, limit, query) {
  return httpGet('admin/userGroup/page', {
    page,
    limit,
    appKey: `${import.meta.env.VITE_APP_KEY}`,
    ...query,
  });
}

export function userGroupAdd(form) {
  return httpPost('admin/userGroup', { ...form, appKey: `${import.meta.env.VITE_APP_KEY}` });
}

export function userGroupEdit(form) {
  return httpPut('admin/userGroup', { ...form, appKey: `${import.meta.env.VITE_APP_KEY}` });
}

export function userGroupDelete(id) {
  return httpDelete(`admin/userGroup/${id}`, { appKey: `${import.meta.env.VITE_APP_KEY}` });
}
