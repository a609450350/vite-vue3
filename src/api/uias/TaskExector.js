import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function jobGroupList(page, limit, query) {
  return httpGet('job/jobGroup/page', {
    page,
    limit,
    ...query,
  });
}

export function jobGroupAdd(form) {
  return httpPost('job/jobGroup', form);
}

export function jobGroupEdit(form) {
  return httpPut('job/jobGroup', form);
}

export function jobGroupDelete(id) {
  return httpDelete(`job/jobGroup/${id}`);
}

export function jobRegistryList(page, limit, query) {
  return httpGet('job/registry/page', {
    page,
    limit,
    ...query,
  });
}

export function jobRegistry(form) {
  return httpPost('job/registry', form);
}

export function jobRegistryDelete(id) {
  return httpDelete(`job/registry/${id}`);
}

export function triggerJob(form) {
  return httpPost('job/taskInfo/triggerJob', form);
}
