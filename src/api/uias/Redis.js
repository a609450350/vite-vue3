import { httpGet, httpPost, httpPut } from '@/api/index';

export function redisList(page, limit, query) {
  return httpGet('admin/redisdata/getKeyList', {
    page,
    limit,
    ...query,
  });
}

export function getKeyDetail(key, type) {
  return httpPost('admin/redisdata/cache/details', {
    redisKey: key,
    db: 0,
    dataType: type,
  });
}

export function deleteKey(key, type) {
  return httpPost('admin/redisdata/cache/delete', {
    redisKey: key,
    db: 0,
    dataType: type,
  });
}
