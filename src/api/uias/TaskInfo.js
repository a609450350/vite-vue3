import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function taskInfoList(page, limit, query) {
  return httpGet('job/taskInfo/page', {
    page,
    limit,
    ...query,
  });
}

export function taskInfoAdd(form) {
  return httpPost('job/taskInfo', form);
}

export function taskInfoEdit(form) {
  return httpPut('job/taskInfo', form);
}

export function taskInfoDelete(id) {
  return httpDelete(`job/taskInfo/${id}`);
}

export function triggerJob(form) {
  return httpPost('job/taskInfo/triggerJob', form);
}

export function nextTriggerTime(id) {
  return httpGet(`job/taskInfo/nextTriggerTime/${id}`);
}

export function startJob(id) {
  return httpPost(`job/taskInfo/startJob/${id}`);
}

export function stopJob(id) {
  return httpDelete(`job/taskInfo/stopJob/${id}`);
}
