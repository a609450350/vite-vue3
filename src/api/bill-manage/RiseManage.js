import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

// 获取列表
export function getBillList(page, limit, query) {
  return httpGet('biz/custAccountInvoice/getList', {
    page,
    limit,
    ...query,
  });
}
// 新增
export function addBill(custId, accountId, query) {
  return httpPost('biz/custAccountInvoice', {
    custId,
    accountId,
    ...query,
  });
}
// 修改
export function updateBill(id, custId, accountId, query) {
  return httpPut('biz/custAccountInvoice/', {
    id,
    custId,
    accountId,
    ...query,
  });
}
// 删除
export function deleteBill(id) {
  return httpDelete(`biz/custAccountInvoice/${id}`);
}

export function getPayDataList(page, limit, name) {
  return httpGet('biz/custIdentity/page', {
    page,
    limit,
    name,
  });
}
