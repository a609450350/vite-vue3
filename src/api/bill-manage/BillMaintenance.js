import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function getByFeeType(feeType) {
  return httpGet('biz/custAccountInvoiceFee/getByFeeType', { ...feeType });
}
export function addFeeType(query) {
  return httpPost('biz/custAccountInvoiceFee', { ...query });
}
export function updateFeeType(query) {
  return httpPut('biz/custAccountInvoiceFee', { ...query });
}
