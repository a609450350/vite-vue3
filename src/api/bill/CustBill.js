import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function custBillList(page, limit, query) {
  return httpGet('biz/custBill/page', {
    page,
    limit,
    ...query,
  });
}

export function custBillAdd(form) {
  return httpPost('biz/custBill', form);
}

export function custBillEdit(form) {
  return httpPut('biz/custBill', form);
}

export function custBillDelete(id) {
  return httpDelete(`biz/custBill/${id}`);
}

export function getCustBillById(id) {
  return httpGet(`biz/custBill/getCustBillById`, { id });
}

export function getCancelBillList(page, limit, query) {
  return httpGet('biz/custBill/getCancelBullList', {
    page,
    limit,
    ...query,
  });
}

export function getCancelBillDetails(id) {
  return httpGet(`biz/custBill/getCancelBullDetails`, { id });
}

export function custBillCancel(form) {
  return httpPost('biz/custBill/addCountermandBill', form);
}
