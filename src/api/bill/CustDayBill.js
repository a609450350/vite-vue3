import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function custDayBillList(page, limit, query) {
  return httpGet('biz/custDayBill/page', {
    page,
    limit,
    ...query,
  });
}

export function custHistoryDayBillList(page, limit, query) {
  return httpGet('biz/custDayBill/getHistorylist', {
    page,
    limit,
    ...query,
  });
}

export function custDayBillAdd(form) {
  return httpPost('biz/custDayBill', form);
}

export function custDayBillEdit(form) {
  return httpPut('biz/custDayBill', form);
}

export function custDayBillDelete(id) {
  return httpDelete(`biz/custDayBill/${id}`);
}

export function getCustDayBillById(id) {
  return httpGet('biz/custDayBill/getCustDayBillById', {
    id,
  });
}
export function getHistoryCustDayBillById(id) {
  return httpGet('biz/custDayBill/getHistoryCustDayBillById', {
    id,
  });
}

export function updateCustDayBillCancel(form) {
  return httpPost('biz/custDayBill/updateCustDayBillCancel', form);
}
