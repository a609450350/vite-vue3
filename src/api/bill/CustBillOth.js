import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function custBillOthList(page, limit, query) {
  return httpGet('biz/custBillOth/page', {
    page,
    limit,
    ...query,
  });
}

export function custBillOthAdd(form) {
  return httpPost('biz/custBillOth', form);
}

export function custBillOthEdit(form) {
  return httpPut('biz/custBillOth', form);
}

export function custBillOthDelete(id) {
  return httpDelete(`biz/custBillOth/${id}`);
}

export function addCancelBillOth(form) {
  return httpPost('biz/custBillOth/addCancelBillOth', form);
}
