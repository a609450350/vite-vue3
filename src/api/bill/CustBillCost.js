import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function custBillCostList(page, limit, query) {
  return httpGet('biz/custBillCost/page', {
    page,
    limit,
    ...query,
  });
}

export function custBillCostAdd(form) {
  return httpPost('biz/custBillCost', form);
}

export function custBillCostEdit(form) {
  return httpPut('biz/custBillCost', form);
}

export function custBillCostDelete(id) {
  return httpDelete(`biz/custBillCost/${id}`);
}

export function addCancelBillCost(form) {
  return httpPost('biz/custBillCost/addCancelBillCost', form);
}
export function CancelBillCostList(page, limit, query) {
  return httpGet('biz/custBillOth/page', {
    page,
    limit,
    ...query,
  });
}
