import { httpGet } from './index';

export function AmapGeoDecode(location) {
  return httpGet('admin/gbCityCode/getGaodeInverseAddrCode', {
    location,
  });
}
