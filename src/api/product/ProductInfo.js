import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function productInfoList(page, limit, query) {
  return httpGet('biz/productInfo/page', {
    page,
    limit,
    ...query,
  });
}

export function productInfoAdd(form) {
  return httpPost('biz/productInfo', form);
}

export function productInfoEdit(form) {
  return httpPut('biz/productInfo', form);
}

export function productInfoDelete(id) {
  return httpDelete(`biz/productInfo/${id}`);
}

export function productInfoPublish(form) {
  return httpPost(`biz/productInfo/updatePublish`, form);
}

export function updateReadjustPrices(form) {
  return httpPost('biz/productInfo/updateReadjustPrices', form);
}

export function getProdValid() {
  return httpGet('biz/productInfo/getProdValid');
}
