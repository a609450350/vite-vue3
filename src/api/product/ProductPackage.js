import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function productPackageList(page, limit, query) {
  return httpGet('biz/productPackage/page', {
    page,
    limit,
    ...query,
  });
}

export function productPackageAdd(form) {
  return httpPost('biz/productPackage', form);
}

export function productPackageEdit(form) {
  return httpPut('biz/productPackage', form);
}

export function productPackageDelete(id) {
  return httpDelete(`biz/productPackage/${id}`);
}

export function productPackagePublish(form) {
  return httpPost(`biz/productPackage/updatePublishPackage`, form);
}

export function getPackageProductList(id) {
  return httpGet(`biz/productPackage/getProdDataByPackId`, { id });
}
export function getProdPackValid() {
  return httpGet(`biz/productPackage/getProdPackValid`);
}

export function updateReadjustPackage(form) {
  return httpPost('biz/productPackage/updateReadjustPackage', form);
}
