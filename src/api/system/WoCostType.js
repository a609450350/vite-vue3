import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function woCostTypeList(page, limit, query) {
  return httpGet('biz/woCostType/page', {
    page,
    limit,
    ...query,
  });
}

export function woCostTypeAdd(form) {
  return httpPost('biz/woCostType', form);
}

export function woCostTypeEdit(form) {
  return httpPut('biz/woCostType', form);
}

export function woCostTypeDelete(id) {
  return httpDelete(`biz/woCostType/${id}`);
}
