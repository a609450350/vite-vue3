import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

// 新增配置
export function AddConfig(data) {
  return httpPost('biz/sysConfFee', data);
}
// 查询配置
export function SelectConfig(page, limit, query) {
  return httpGet('biz/sysConfFee/page', {
    page,
    limit,
    ...query,
  });
}
// 查看配置详情
export function SelectDetails(page, limit, query) {
  return httpGet('biz/sysConfFeeAddr/getListForId', {
    page,
    limit,
    ...query,
  });
}
// 编辑配置
export function EditConfig(data) {
  return httpPut('biz/sysConfFee', data);
}
// 删除配置
export function DeleteConfig(id) {
  return httpDelete(`biz/sysConfFee/${id}`);
}
// 修改配置
export function UpdateConfig(data) {
  return httpPut('biz/sysConfFee', data);
}
// 查询编册列表
export function SelectRegister(page, limit, query) {
  return httpGet('biz/sysConfFeeAddr/getAddrInfoListByParam', {
    page,
    limit,
    ...query,
  });
}
// 删除编册列表
export function DeleteRegister(id) {
  return httpDelete(`biz/sysConfFeeAddr/${id}`);
}
// 新增编册列表
export function AddRegister(data) {
  return httpPost('biz/sysConfFeeAddr/addList', data);
}
// 修改状态
export function sysConfFeeStatus(id, state) {
  return httpPut('biz/sysConfFee/updateState', { id, state });
}
