import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function sysAnnouncementList(page, limit, query) {
  return httpGet('biz/sysAnnouncement/page', {
    page,
    limit,
    ...query,
  });
}

export function sysAnnouncementAdd(form) {
  return httpPost('biz/sysAnnouncement', form);
}

export function sysAnnouncementEdit(form) {
  return httpPut('biz/sysAnnouncement', form);
}

export function sysAnnouncementDelete(id) {
  return httpDelete(`biz/sysAnnouncement/${id}`);
}
