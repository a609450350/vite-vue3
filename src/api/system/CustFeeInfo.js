import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function custFeeInfoList(page, limit, query) {
  return httpGet('biz/custFeeInfo/page', {
    page,
    limit,
    ...query,
  });
}

export function custFeeInfoListAll() {
  return httpGet('biz/custFeeInfo/all', {});
}

export function custFeeInfoAdd(form) {
  return httpPost('biz/custFeeInfo', form);
}

export function custFeeInfoEdit(form) {
  return httpPut('biz/custFeeInfo', form);
}

export function custFeeInfoDelete(id) {
  return httpDelete(`biz/custFeeInfo/${id}`);
}
