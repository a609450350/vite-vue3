import { httpGet, httpPost, httpPut } from '@/api/index';

// 地址信息列表查询
export function getAnnexAddrList(page, limit, query) {
  return httpGet('biz/addrInfo/getAnnexAddrList', {
    page,
    limit,
    openAccount: '1004701',
    ...query,
  });
}
// 客户附件资料列表查询
export function getListByParam(page, limit, addrId, custIdentityId) {
  return httpGet('biz/custAccessoryFile/getListByParam', {
    page,
    limit,
    addrId,
    custIdentityId,
  });
}
// 新增附件
export function custAccessoryFileAdd(addrId, custIdentityId, remark, fileUrl, fileType) {
  return httpPost('biz/custAccessoryFile', {
    addrId,
    custIdentityId,
    remark,
    fileUrl,
    fileType,
  });
}
// 修改状态
export function UpdateStatus(id, state) {
  return httpPut('biz/custAccessoryFile', { id, state });
}
