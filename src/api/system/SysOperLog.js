import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function sysOperLogList(page, limit, query) {
  return httpGet('biz/sysOperLog/page', {
    page,
    limit,
    ...query,
  });
}

export function sysOperLogAdd(form) {
  return httpPost('biz/sysOperLog', form);
}

export function sysOperLogEdit(form) {
  return httpPut('biz/sysOperLog', form);
}

export function sysOperLogDelete(id) {
  return httpDelete(`biz/sysOperLog/${id}`);
}
