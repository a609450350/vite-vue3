import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function custAccountTempList(page, limit, query) {
  return httpGet('biz/custAccountTemp/page', {
    page,
    limit,
    ...query,
  });
}

export function custAccountTempAdd(form) {
  return httpPost('biz/custAccountTemp', form);
}

export function custAccountTempEdit(form) {
  return httpPut('biz/custAccountTemp', form);
}

export function custAccountTempDelete(id) {
  return httpDelete(`biz/custAccountTemp/${id}`);
}

export function getTempByCompanyId(tempType) {
  return httpGet('biz/custAccountTemp/getTempByCompanyId', { tempType });
}

export function getPrintPayById(urlCode, payId) {
  return httpGet('biz/custAccountTemp/getPrintPayById', {
    urlCode,
    payId,
  });
}
// 1012401 缴费充值
