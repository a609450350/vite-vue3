import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function sysFeedbackList(page, limit, query) {
  return httpGet('biz/sysFeedback/page', {
    page,
    limit,
    ...query,
  });
}

export function sysFeedbackAdd(form) {
  return httpPost('biz/sysFeedback', form);
}

export function sysFeedbackEdit(form) {
  return httpPut('biz/sysFeedback', form);
}

export function sysFeedbackDelete(id) {
  return httpDelete(`biz/sysFeedback/${id}`);
}
