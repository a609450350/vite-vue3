import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function sysInformationList(page, limit, query) {
  return httpGet('biz/sysInformation/page', {
    page,
    limit,
    ...query,
  });
}

export function sysInformationAdd(form) {
  return httpPost('biz/sysInformation', form);
}

export function sysInformationEdit(form) {
  return httpPut('biz/sysInformation', form);
}

export function sysInformationDelete(id) {
  return httpDelete(`biz/sysInformation/${id}`);
}
