import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function sysConfSubsidyList(page, limit, query) {
  return httpGet('biz/sysConfSubsidy/page', {
    page,
    limit,
    ...query,
  });
}

export function sysConfSubsidyAdd(form) {
  return httpPost('biz/sysConfSubsidy', form);
}

export function sysConfSubsidyEdit(form) {
  return httpPut('biz/sysConfSubsidy', form);
}

export function sysConfSubsidyDelete(id) {
  return httpDelete(`biz/sysConfSubsidy/${id}`);
}

export function getSubsidyAddrList(id, page, limit, query) {
  return httpGet('biz/sysConfSubsidy/getSubsidyAddrList', {
    id,
    page,
    limit,
    ...query,
  });
}
