import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function sysConfDataList(page, limit, query) {
  return httpGet('biz/sysConfData/page', {
    page,
    limit,
    ...query,
  });
}

export function sysConfDataHistoryList(page, limit, query) {
  return httpGet('biz/sysConfData/getHistoryList', {
    page,
    limit,
    ...query,
  });
}

export function sysConfDataAdd(form) {
  return httpPost('biz/sysConfData', form);
}

export function sysConfDataEdit(form) {
  return httpPut('biz/sysConfData', form);
}

export function sysConfDataDelete(id) {
  return httpDelete(`biz/sysConfData/${id}`);
}

export function getSysConfDataDicAll() {
  return httpGet('biz/sysConfData/getSysConfDicAll', {});
}
