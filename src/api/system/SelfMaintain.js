import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function getSelfList(page, limit, query) {
  return httpGet('biz/busUserDevice/page', { page, limit, ...query });
}

export function busUserDevice(data) {
  return httpPost('biz/busUserDevice', data);
}

export function deleteSelf(id) {
  return httpDelete(`biz/busUserDevice/${id}`);
}

export function editSelf(data) {
  return httpPut('biz/busUserDevice/', data);
}
