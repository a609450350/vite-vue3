import { httpGet, httpPost, httpPut } from '@/api/index';

export function getBizDicListByParams(page, limit, query) {
  return httpGet('biz/dic/getBizDicListByParams', {
    page,
    limit,
    ...query,
  });
}

export function addCisBizDic(form) {
  return httpPost('biz/dic/addCisBizDic', form);
}

export function updateCisBizDic(form) {
  return httpPut('biz/dic/updateCisBizDic', form);
}
