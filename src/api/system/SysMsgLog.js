import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function sysMsgLogList(page, limit, query) {
  return httpGet('biz/sysMsgLog/page', {
    page,
    limit,
    ...query,
  });
}

export function sysMsgLogAdd(form) {
  return httpPost('biz/sysMsgLog', form);
}

export function sysMsgLogEdit(form) {
  return httpPut('biz/sysMsgLog', form);
}

export function sysMsgLogDelete(id) {
  return httpDelete(`biz/sysMsgLog/${id}`);
}
