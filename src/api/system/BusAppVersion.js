import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function busAppVersionList(page, limit, query) {
  return httpGet('biz/busAppVersion/page', {
    page,
    limit,
    ...query,
  });
}

export function busAppVersionAdd(form) {
  return httpPost('biz/busAppVersion', form);
}

export function busAppVersionEdit(form) {
  return httpPut('biz/busAppVersion', form);
}

export function busAppVersionDelete(id) {
  return httpDelete(`biz/busAppVersion/${id}`);
}
