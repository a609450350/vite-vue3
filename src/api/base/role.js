import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

// 角色管理

export function roleList() {
  return httpGet('admin/group/getGropList');
}

export function addRole(role) {
  // 新增角色
  return httpPost('baseGroup', role);
}

export function editRole(role) {
  // 修改角色
  return httpPut('baseGroup', role);
}

export function deleteRole(id) {
  // 删除角色
  return httpDelete(`baseGroup/${id}`);
}

export function roleType() {
  // 角色类型
  return httpGet('baseGroupType/all');
}

export function menuAuthorityList(id = -1) {
  return httpGet(`baseGroup/authority/menu/${id}`);
}

export function grantedUserList(id = -1) {
  // 已授权角色的用户列表
  return httpGet(`baseGroup/user/${id}`);
}

export function grantRoleToMenu(form) {
  // 授权角色到菜单
  return httpPut('baseGroup/authority/menu', form);
}

export function grantRoleToUser(id, members, type = 'user') {
  // 授权角色到用户
  return httpPut('baseGroup/user', {
    id,
    members,
    type,
  });
}
export function grantRoleToUserbaseGroup(id, members, type = 'baseGroup') {
  // 授权角色到用户
  return httpPut('baseGroup/user', {
    id,
    members,
    type,
  });
}
