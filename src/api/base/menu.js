import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

// 菜单管理

export function elementTree() {
  return httpGet('baseMenu/getEleTreeUser');
}

export function menuTree() {
  // 菜单管理树
  return httpGet('baseMenu/tree');
}

export function addMenu(menu) {
  // 添加菜单
  return httpPost('baseMenu', menu);
}

export function editMenu(menu) {
  // 编辑菜单
  return httpPut('baseMenu', menu);
}

export function deleteMenu(id) {
  // 删除菜单
  return httpDelete(`baseMenu/${id}`);
}

// ------------------------------------------------ //

// 菜单资源管理

export function elementList(menuId, name = '') {
  // 资源列表
  return httpGet('baseElement/list', { menuId, name });
}

export function addElement(form) {
  return httpPost('element', form);
}

export function editElement(form) {
  return httpPut('element', form);
}

export function deleteElement(id) {
  return httpDelete(`baseElement/${id}`);
}
