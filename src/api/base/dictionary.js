/*
 * @Author: Cui
 * @Date: 2020-09-23 10:20:42
 * @LastEditTime: 2020-09-23 10:30:21
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: /ch-uiap-ui/src/api/dictionary.js
 */
// import { httpGet, httpPost, httpDelete, httpPut} from '@/api/index'
import { httpGet, httpPost, httpPut } from '@/api/index';

// 字典管理

export function dictionaryTree() {
  // 获取字典树
  return httpGet('dictionary/getDictionaryTree', {});
}

export function dictionaryList() {
  // 获取字典列表
  return httpGet('dictionary/getDictionaryList', {});
}

export function addDictionary(form) {
  // 新增字典
  return httpPost('dictionary', form);
}

export function editDictionary(form) {
  // 修改字典
  return httpPut('dictionary', form);
}

export function findDictionary(pid) {
  return httpGet(`dictionary/getDicByPid/${pid}`);
}
