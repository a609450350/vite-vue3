import { httpGet, httpPost, httpPut } from '@/api/index';

export function baseTenantList(page = 1, limit = 10) {
  return httpGet('baseTenant/page', {
    page,
    limit,
  });
}

export function baseTenantAll() {
  return httpGet('baseTenant/all');
}

export function addBaseTenant(form) {
  return httpPost('baseTenant', form);
}

export function editBaseTenant(form) {
  return httpPut('baseTenant', form);
}
