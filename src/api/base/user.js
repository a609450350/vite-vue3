import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

// 账号是否可用
export function countByUsername(username) {
  return httpGet(`jwt/countByUsername/${username}`);
}

// 获取手机号是否占用
export function getUserByTelPhone(phone) {
  return httpGet(`jwt/getUserByTelPhone/${phone}`);
}

// 发送验证码
export function addSendMsg(phone) {
  return httpGet(`auth/jwt/forgetPwd/addSendMsg/${phone}`);
}

// 注册
export function registerUser(data) {
  return httpPost('jwt/registerUser', data);
}

// 忘记密码
export function updatePwd(data) {
  return httpPost('jwt/forgetPwd/updatePwd', data);
}

// 短信验证码登录
export function phoneLogin(data) {
  return httpPost('jwt/validateByCode', data);
}

// 人员管理

export function userList(page = 1, limit = 10, query) {
  return httpGet('baseUser/page', {
    page,
    limit,
    ...query,
  });
}

export function userListAll() {
  return httpGet('baseUser/all');
}

export function addUser(form) {
  return httpPost('baseUser', form);
}

export function editUser(form) {
  return httpPut('baseUser', form);
}

export function deleteUser(id) {
  return httpDelete(`baseUser/${id}`);
}

export function userPwdReset(username) {
  return httpPost('baseUser/resetPwd', { username });
}

export function changePwd(form) {
  return httpPut('baseUser/changePwd', form);
}
