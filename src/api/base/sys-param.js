import { httpGet, httpPost, httpPut } from '@/api/index';

// 系统参数
export function sysParamList(page = 1, limit = 10) {
  return httpGet('baseSysParams/page', {
    page,
    limit,
  });
}

export function addSysParam(form) {
  return httpPost('baseSysParams', form);
}

export function editSysParam(form) {
  return httpPut('baseSysParams', form);
}

export function querySysParam(key) {
  return httpGet(`baseSysParams/getSysParamsByKey/${key}`);
}
