/*
 * @Author: Cui
 * @Date: 2020-09-23 10:21:09
 * @LastEditTime: 2020-09-23 10:38:52
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: /ch-uiap-ui/src/api/application.js
 */
import { httpGet, httpPost, httpPut } from '@/api/index';

// 应用管理
export function applicationList(page = 1, limit = 10) {
  return httpGet('application/page', {
    page,
    limit,
  });
}

export function applicationListAll() {
  return httpGet('application/all');
}

export function addApplication(form) {
  return httpPost('application', form);
}

export function editApplication(form) {
  return httpPut('application', form);
}

export function grantAppToTenant(tenantId, appIds) {
  return httpPost('application/addAppTenant', {
    tenantId,
    appIds,
  });
}

export function grantedAppList(id) {
  return httpGet(`tenant/getAuthApp/${id}`);
}

export function authedAppList() {
  return httpGet('application/getAuthAppByTentId');
}

export function getAppAuthTenantList() {
  // 该应用授权的租户
  return httpGet(`baseTenant/all`);
}
