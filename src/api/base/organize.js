import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

// 组织管理

export function organizeUserList(orgId, name = '') {
  // 组织下人员列表
  return httpGet('baseUser/getUserByOrgId', {
    orgId,
    name,
  });
}

export function organizeTree(appKey, tentId) {
  // 获取组织树
  return httpGet('baseOrg/getOrgTree', {
    appKey,
    tentId,
  });
}

export function addOrganize(form) {
  // 添加组织
  return httpPost('baseOrg', form);
}

export function editOrganize(form) {
  // 修改组织
  return httpPut('baseOrg', form);
}

export function deleteOrganize(id) {
  // 删除组织
  return httpDelete(`baseOrg/${id}`);
}

export function grantedOrganizeUserList(id) {
  // 组织授权用户列表
  return httpGet(`baseUserOrg/getUserByOrgId/${id}`);
}

export function grantOrganize(orgId, userIds) {
  // 组织授权
  return httpPost('baseUserOrg', { userIds, orgId });
}

export function getAllOrgTree() {
  return httpGet(`admin/organization/getAllOrgTree?type=user`);
}
