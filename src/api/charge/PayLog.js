import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function payLogList(page, limit, query) {
  return httpGet('biz/payLog/page', {
    page,
    limit,
    ...query,
  });
}

export function payLogAdd(form) {
  return httpPost('biz/payLog', form);
}

export function payLogEdit(form) {
  return httpPut('biz/payLog', form);
}

export function payLogDelete(id) {
  return httpDelete(`biz/payLog/${id}`);
}

export function addRefund(form) {
  return httpPost('biz/payLog/addRefund', form);
}
