import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function custPayReconList(page, limit, query) {
  return httpGet('biz/custPayRecon/page', {
    page,
    limit,
    ...query,
  });
}

export function custPayReconAdd(form) {
  return httpPost('biz/custPayRecon', form);
}

export function custPayReconEdit(form) {
  return httpPut('biz/custPayRecon', form);
}

export function custPayReconDelete(id) {
  return httpDelete(`biz/custPayRecon/${id}`);
}

export function cancelPayRecon(id) {
  return httpPost('biz/custPayRecon/cancelPayRecon', { id });
}

export function getPayReconCount(reconType) {
  return httpGet('biz/custPayRecon/getPayReconCount', {
    reconType,
  });
}

export function getPayReconById(id) {
  return httpGet('biz/custPayRecon/getPayReconById', {
    id,
  });
}
