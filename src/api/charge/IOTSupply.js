import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

// 列表查询
export function getIotDataList(page, limit, query) {
  return httpGet('biz/addrMeter/remoteAirList', {
    page,
    limit,
    ...query,
  });
}
// 新增补量
export function addIotData(id, addrId, data) {
  return httpPost('biz/addrMeter/remoteAirAdd', { id, addrId, ...data });
}
// 补量记录查询
export function getIotRecordList(page, limit, query) {
  return httpGet('biz/bizSmartMeterOperLog/page', {
    page,
    limit,
    ...query,
  });
}
// 扣量新增
export function remoteAddIotData(id, addrId, data) {
  return httpPost('biz/addrMeter/remoteAirSubtract', { id, addrId, ...data });
}
