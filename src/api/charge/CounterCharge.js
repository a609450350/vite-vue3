import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function getPayDataList(page, limit, name) {
  return httpGet('biz/custPayInfo/getPayDataList', {
    page,
    limit,
    name,
  });
}

export function addDepositMoney(form) {
  return httpPost('biz/custPayInfo/addDepositMoney', form);
}

export function getCustDetails(id) {
  return httpGet('biz/custPayInfo/getCustDetails', { id });
}

export function addCounterPayFee(form) {
  return httpPost('biz/custPayInfo/addCounterPayFee', form);
}

export function getPayCountDay() {
  return httpGet('biz/custPayInfo/getPayCountDay');
}

// 计算后付费
export function getAmountPayable2(form) {
  return httpPost('biz/custPayInfo/getAmountPayable2', form);
}

// 取消收费
export function getCountermandPayDataList(page, limit, query) {
  return httpGet('biz/custPayInfo/getCountermandPayDataList', {
    page,
    limit,
    ...query,
  });
}

export function getCountermandPayDetails(id) {
  return httpGet('biz/custPayInfo/getCountermandPayDetails', { id });
}

export function addCountermandPay(form) {
  return httpPost('biz/custPayInfo/addCountermandPay', form);
}

// IC卡取消收费
export function getIcCountermandPayDataList(page, limit, query) {
  return httpGet('biz/custPayInfo/getIcCountermandPayDataList', {
    page,
    limit,
    ...query,
  });
}

export function addIcCancelRecharge(form) {
  return httpPost('biz/custPayInfo/addIcCancelRecharge', form);
}
