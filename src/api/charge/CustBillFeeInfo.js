import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function custBillFeeInfoList(page, limit, query) {
  return httpGet('biz/custBillFeeInfo/page', {
    page,
    limit,
    ...query,
    source: 1007101,
  });
}

export function custBillFeeInfoAdd(form) {
  return httpPost('biz/custBillFeeInfo', form);
}

export function custBillFeeInfoEdit(form) {
  return httpPut('biz/custBillFeeInfo', form);
}

export function custBillFeeInfoDelete(id) {
  return httpDelete(`biz/custBillFeeInfo/${id}`);
}

export function getOpenAddrList(page, limit, query) {
  return httpGet(`biz/addrInfo/getOpenAddrList`, {
    page,
    limit,
    ...query,
  });
}
export function getOpenAddrListByAddrId(page, limit, query) {
  return httpGet('biz/addrInfo/page', {
    page,
    limit,
    openAccount: '1004701',
    ...query,
  });
}
// 批量新增
export function custBillFeeInfoBatchAdd(data) {
  return httpPost('biz/custBillFeeInfo/addList', data);
}
export function getFeeTypeList(feeType) {
  return httpGet(`biz/custFeeInfo/selectByType?feeType=${feeType}`);
}
