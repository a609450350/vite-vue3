import { httpGet, httpPost } from '@/api/index';

export function getPayDataList(page, limit, name) {
  return httpGet('biz/custPayInfo/getPayIotMeterDataList', {
    page,
    limit,
    name,
  });
}

export function addInternetMeterPayFee(form) {
  return httpPost('biz/custPayInfo/addInternetMeterPayFee', form);
}
