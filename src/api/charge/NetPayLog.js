import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function netPayLogList(page, limit, query) {
  return httpGet('biz/netPayLog/page', {
    page,
    limit,
    ...query,
  });
}

export function netPayLogAdd(form) {
  return httpPost('biz/netPayLog', form);
}

export function netPayLogEdit(form) {
  return httpPut('biz/netPayLog', form);
}

export function netPayLogDelete(id) {
  return httpDelete(`biz/netPayLog/${id}`);
}

export function addRefund(form) {
  return httpPost('biz/netPayLog/addRefund', form);
}
