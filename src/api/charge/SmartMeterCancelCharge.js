import { httpGet, httpPost } from '@/api/index';

export function getRefundMeterList(page, limit, query) {
  return httpGet('biz/custPayInfo/getRefundMeterList', {
    page,
    limit,
    ...query,
  });
}

export function addRefundMeter(form) {
  return httpPost('biz/custPayInfo/addRefundMeter', form);
}
