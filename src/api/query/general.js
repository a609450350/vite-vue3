import { httpGet } from '@/api/index';

export function getBizOperationLogList(page, limit, query) {
  return httpGet('biz/search/getBizOperationLogList', {
    page,
    limit,
    ...query,
  });
}

export function addrInfoList(page, limit, query) {
  return httpGet('biz/search/getAddrInfoList', {
    page,
    limit,
    ...query,
  });
}

export function getCustIdentityList(page, limit, query) {
  return httpGet('biz/search/getCustIdentityList', {
    page,
    limit,
    ...query,
  });
}

export function getCustIdentityData(id) {
  return httpGet('biz/search/getCustIdentityData', {
    id,
  });
}

export function getCountermandPayDetails(id) {
  return httpGet('biz/custPayInfo/getCountermandPayDetails', { id });
}

export function getReceiptsList(page, limit, query) {
  return httpGet('biz/custPayInfo/getReceiptsList', {
    page,
    limit,
    ...query,
  });
}

export function getReceivableList(page, limit, query) {
  return httpGet('biz/custBillFeeInfo/getReceivableList', {
    page,
    limit,
    ...query,
  });
}

export function getReceivableDetails(id) {
  return httpGet('biz/custBillFeeInfo/getReceivableDetails', { id });
}

export function getAddrInfoDataById(id) {
  return httpGet('biz/search/getAddrInfoDataById', { id });
}

export function smartTableQueryList(page, limit, query) {
  return httpGet('biz/bizSmartMeterOperLog/selectListByParam', {
    page,
    limit,
    ...query,
  });
}

export function meterInstallList(page, limit, data) {
  return httpGet('biz/addrMeter/installationList', {
    page,
    limit,
    ...data,
  });
}
export function meterInstallInfo(id) {
  return httpGet(`biz/addrMeter/installationById?id=${id}`);
}
