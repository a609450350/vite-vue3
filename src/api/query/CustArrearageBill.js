import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function custBillList(page, limit, query) {
  return httpGet('biz/custBill/getArrearageCustList', {
    page,
    limit,
    ...query,
  });
}

export function getArrearageCustDetails(id) {
  return httpGet('biz/custBill/getArrearageCustDetails', { id });
}

export function addAskPayment(id) {
  return httpGet('biz/custBill/addAskPayment', { id });
}
