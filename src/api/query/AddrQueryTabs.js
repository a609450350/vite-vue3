import { httpGet } from '@/api/index';

// 安装表具列表
export function meterListByParams(id, meterCode = '', type) {
  return httpGet('biz/search/meterListByParam', { id, meterCode, type });
}
// 产品信息
export function getCustIdentityData(id, addrId) {
  return httpGet('biz/search/getCustIdentityData', { id, addrId });
}

// 账单详情
export function getCustBillById(id) {
  return httpGet('biz/custBill/getCustBillById', { id });
}
// 表具预存列表
export function selectRsList(page, limit, query) {
  return httpGet('biz/search/selectRsList', {
    page,
    limit,
    ...query,
  });
}

// 费用账单查询
export function custBillList(page, limit, query) {
  return httpGet('biz/custBill/page', {
    page,
    limit,
    ...query,
  });
}
// 其他收费账单
export function custBillOthList(page, limit, query) {
  return httpGet('biz/custBillOth/page', {
    page,
    limit,
    ...query,
  });
}
// 材料费账单
export function custBillCostList(page, limit, query) {
  return httpGet('biz/custBillCost/page', {
    page,
    limit,
    ...query,
  });
}

// 日账单
export function custDayBillList(page, limit, query) {
  return httpGet('biz/custDayBill/page', {
    page,
    limit,
    ...query,
  });
}
// 业务办理记录
export function getBizOperationLogList(page, limit, query) {
  return httpGet('biz/search/getBizOperationLogList', {
    page,
    limit,
    ...query,
  });
}
// 智能表查询
export function SelectList(page, limit, query) {
  return httpGet('biz/bizSmartMeterOperLog/selectListByParam', {
    page,
    limit,
    ...query,
  });
}
// 账户流水
export function custAccountFlowList(page, limit, query) {
  return httpGet('biz/custAccountFlow/page', {
    page,
    limit,
    type: 0,
    ...query,
  });
}
// 付款记录
export function getReceiptsList(page, limit, query) {
  return httpGet('biz/custPayInfo/getReceiptsList', {
    page,
    limit,
    ...query,
  });
}
// 抄表记录
export function mrRecordList(page, limit, query) {
  return httpGet('biz/mrRecord/page', {
    page,
    limit,
    ...query,
  });
}
// 采集记录
export function iotCollectionRecordsList(page, limit, query) {
  return httpGet('iotsyn/iotCollectionRecords/getCollectionRecordsByMeterId', {
    page,
    limit,
    ...query,
  });
}
// 远传操作
export function iotOptDicLogList(page, limit, query) {
  return httpGet('iotsyn/iotOptDicLog/page', {
    page,
    limit,
    ...query,
  });
}
