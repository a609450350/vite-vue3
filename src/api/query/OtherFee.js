import { httpGet } from '@/api/index';

export function getOtherFeeList(page, limit, query) {
  return httpGet('biz/sysConfFeeAddr/getSysConfFeeAddrFee', {
    page,
    limit,
    ...query,
  });
}
