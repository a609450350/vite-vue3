import { httpDelete, httpGet, httpPost, httpPostForm, httpPut } from '@/api/index';

export function getAuditWorkOrderList(page, limit, query) {
  return httpGet('biz/mrWorkOrder/getAuditWorkOrderList', {
    page,
    limit,
    ...query,
  });
}

export function getAuditMrRecordList(id) {
  return httpGet(`biz/mrWorkOrder/getAuditMrRecordList`, { id });
}

export function addAuditWorkOrder(form) {
  return httpPost(`biz/mrWorkOrder/addAuditWorkOrder`, form);
}

export function batchCheckMr(form) {
  return httpPost(`biz/mrWorkOrder/batchCheckMr`, form);
}

export function addBatchAuditWorkOrder(form) {
  return httpPost(`biz/mrWorkOrder/addBatchAuditWorkOrder`, form);
}
