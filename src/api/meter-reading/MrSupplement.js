import { httpGet, httpPost } from '@/api/index';

export function mrSupplementList(page, limit, query) {
  return httpGet('biz/mrRecord/getRecordCollection', {
    page,
    limit,
    ...query,
  });
}

export function mrSupplementAdd(form) {
  return httpPost('biz/mrRecord/addRecordCollection', form);
}
