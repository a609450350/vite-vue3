import { httpDelete, httpGet, httpPost, httpPostForm, httpPut } from '@/api/index';

export function getAuditWorkOrderList(page, limit, query) {
  return httpGet('biz/mrRoute/page', {
    page,
    limit,
    ...query,
  });
}
export function getAuditMrRecordList(page, limit, code, query) {
  return httpGet('biz/mrRoute/getAddrListById', {
    page,
    limit,
    code,
    ...query,
  });
}
export function addAuditWorkOrder(form) {
  return httpPost(`biz/mrWorkOrder/addAuditWorkOrder`, form);
}
