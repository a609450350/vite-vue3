import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function getTaskList(page, limit, query) {
  return httpGet('biz/mrWorkOrder/page', {
    page,
    limit,
    ...query,
  });
}

export function getTaskDesList(id, page, limit, query) {
  return httpGet('biz/mrWorkOrder/getWorkOrderAddrById', {
    id,
    page,
    limit,
    ...query,
  });
}

export function getMrWorkOrderById(id) {
  return httpGet('biz/mrWorkOrder/getMrWorkOrderById', { id });
}

export function changeTask(form) {
  return httpGet('biz/mrWorkOrder/addReass', form);
}

export function buildTask(form) {
  return httpPost('biz/mrWorkOrder/addMrWorkOrder', form);
}

export function closeTask(id) {
  return httpPost('biz/mrWorkOrder/closeMrWorkOrder', { id });
}
