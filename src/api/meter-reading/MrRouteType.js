import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function mrRouteTypeList(page, limit, query) {
  return httpGet('biz/mrRouteType/page', {
    page,
    limit,
    ...query,
  });
}

export function mrRouteTypeAdd(form) {
  return httpPost('biz/mrRouteType', form);
}

export function mrRouteTypeEdit(form) {
  return httpPut('biz/mrRouteType', form);
}

export function mrRouteTypeDelete(id) {
  return httpDelete(`biz/mrRouteType/${id}`);
}
