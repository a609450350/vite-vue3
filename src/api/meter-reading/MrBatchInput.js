import { httpDelete, httpGet, httpPost, httpPostForm, httpPut } from '@/api/index';

export function mrOrderList(page, limit, query) {
  return httpGet('biz/mrWorkOrder/getEnteringWorkOrderList', {
    page,
    limit,
    ...query,
  });
}

export function getMrRecordById(id) {
  return httpGet(`biz/mrRecord/getMrRecordById`, { id });
}

export function getExportEnteringWorkOrderList(id) {
  return httpGet(`biz/mrWorkOrder/getExportEnteringWorkOrderList`, { id });
}

export function addEnteringWorkOrderFile(form) {
  return httpPostForm(`biz/mrWorkOrder/addEnteringWorkOrderFile`, form);
}

export function addEnteringWorkOrderList(form) {
  return httpPost(`biz/mrWorkOrder/addEnteringWorkOrderList`, form);
}

export function getEnteringWorkOrderDetailsList(id) {
  return httpGet(`biz/mrWorkOrder/getEnteringWorkOrderDetailsList`, { id });
}

export function getEnteringWorkOrderDetailsById(womId, id) {
  return httpGet(`biz/mrWorkOrder/getEnteringWorkOrderDetailsById`, { womId, id });
}
export function addWorkOrderMr(form) {
  return httpPost(`biz/mrWorkOrder/addWorkOrderMr`, form);
}
