import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function mrRecordList(page, limit, query) {
  return httpGet('biz/mrRecord/page', {
    page,
    limit,
    ...query,
  });
}

export function mrRecordAdd(form) {
  return httpPost('biz/mrRecord', form);
}

export function mrRecordEdit(form) {
  return httpPut('biz/mrRecord', form);
}

export function mrRecordDelete(id) {
  return httpDelete(`biz/mrRecord/${id}`);
}

export function getMrRecordById(id) {
  return httpGet(`biz/mrRecord/getMrRecordById`, { id });
}
