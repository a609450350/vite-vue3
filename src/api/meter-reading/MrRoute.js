import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function mrRouteList(page, limit, query) {
  return httpGet('biz/mrRoute/page', {
    page,
    limit,
    ...query,
  });
}

export function mrRouteAdd(form) {
  return httpPost('biz/mrRoute', form);
}

export function mrRouteEdit(form) {
  return httpPut('biz/mrRoute', form);
}

export function mrRouteDelete(id) {
  return httpDelete(`biz/mrRoute/${id}`);
}

export function getAddrInfoByRoteCode(page, limit, query) {
  return httpGet('biz/mrRecord/getAddrInfoByRoteCode', {
    page,
    limit,
    ...query,
  });
}

export function getAddrInfoRouteByAddrId(addrId, addrMeterId) {
  return httpGet('biz/mrRecord/getAddrInfoRouteByAddrId', {
    addrId,
    addrMeterId,
  });
}

export function addRouteMrRecord(form) {
  return httpPost('biz/mrRecord/addRouteMrRecord', form);
}

export function getNotOutMrRecordByRouteId(page, limit, query) {
  return httpGet('biz/mrRecord/getNotOutMrRecordByRouteId', {
    page,
    limit,
    ...query,
  });
}

export function getNotOutMrRecordById(addrId, addrMeterId) {
  return httpGet('biz/mrRecord/getNotOutMrRecordById', {
    addrId,
    addrMeterId,
  });
}

export function addBillRecord(form) {
  return httpPost('biz/mrRecord/addBillRecord', form);
}
