import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function mrRouteAddrList(page, limit, query) {
  return httpGet('biz/mrRoute/getRouteAddrList', {
    page,
    limit,
    ...query,
  });
}

export function addRouteAddr(form) {
  return httpPost('biz/mrRoute/addRouteAddr', form);
}

export function addRouteBuilding(form) {
  return httpPost('biz/mrRoute/addRouteBuilding', form);
}

export function mrRouteAddrDelete(addrIds) {
  return httpPost(`biz/mrRoute/delRouteAddr`, { addrIds });
}

export function getAddRouteBuildingrList(page, limit, query) {
  return httpGet('biz/mrRoute/getAddRouteBuildingrList', {
    page,
    limit,
    ...query,
  });
}

export function getAddRouteAddrList(page, limit, query) {
  return httpGet('biz/mrRoute/getAddRouteAddrList', {
    page,
    limit,
    ...query,
  });
}
