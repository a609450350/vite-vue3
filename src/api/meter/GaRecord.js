import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function gaRecordList(page, limit, query) {
  return httpGet('biz/gaRecord/page', {
    page,
    limit,
    ...query,
  });
}

export function gaRecordAdd(form) {
  return httpPost('biz/gaRecord', form);
}

export function gaRecordEdit(form) {
  return httpPut('biz/gaRecord', form);
}

export function gaRecordDelete(id) {
  return httpDelete(`biz/gaRecord/${id}`);
}
