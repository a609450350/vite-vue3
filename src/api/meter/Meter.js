import { httpDelete, httpGet, httpPost, httpPostForm, httpPut } from '@/api/index';

export function meterList(page, limit, query) {
  return httpGet('biz/meter/page', {
    page,
    limit,
    ...query,
  });
}

export function getMeterByNo(meterCode) {
  return httpGet('biz/meter/getMeterByNo', {
    meterCode,
  });
}

export function meterAdd(form) {
  return httpPost('biz/meter', form);
}

export function meterEdit(form) {
  return httpPut('biz/meter', form);
}

export function meterDelete(id) {
  return httpDelete(`biz/meter/${id}`);
}

export function getMeterTemplate() {
  return httpGet('biz/meter/getBatchMeterExportTemp');
}

export function addBatchMeterImportTemp(form) {
  return httpPostForm('biz/meter/addBatchMeterImportTemp', form);
}

export function addBatchMeter(form) {
  return httpPost('biz/meter/addBatchMeter', form);
}

export function getMeterVerify(code) {
  return httpGet('biz/meter/getMeterVerify', { code });
}
// 获取表具编号
export function getMeterCode() {
  return httpGet('biz/meter/getCreateMeterNo');
}
