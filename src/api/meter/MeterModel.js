import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function meterModelList(page, limit, query) {
  return httpGet('biz/meterModel/page', {
    page,
    limit,
    ...query,
  });
}

export function meterModelListAll() {
  return httpGet('biz/meterModel/all');
}

export function meterModelAdd(form) {
  return httpPost('biz/meterModel', form);
}

export function meterModelEdit(form) {
  return httpPut('biz/meterModel', form);
}

export function meterModelDelete(id) {
  return httpDelete(`biz/meterModel/${id}`);
}
