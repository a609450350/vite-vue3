import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function iotSysConfList(page, limit, query) {
  return httpGet('biz/iotSysConf/page', {
    page,
    limit,
    ...query,
  });
}

export function iotSysConfHistoryList(page, limit, query) {
  return httpGet('biz/iotSysConf/getHistoryList', {
    page,
    limit,
    ...query,
  });
}

export function iotSysConfAdd(form) {
  return httpPost('biz/iotSysConf', form);
}

export function iotSysConfEdit(form) {
  return httpPut('biz/iotSysConf', form);
}

export function iotSysConfDelete(id) {
  return httpDelete(`biz/iotSysConf/${id}`);
}

export function getIotSysConfDicAll() {
  return httpGet('biz/iotSysConf/getIotSysConfDicAll', {});
}
