import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function getNetMeterCount(page, limit, query) {
  return httpGet('iotsyn/deviceInfo/page', {
    page,
    limit,
    ...query,
  });
}
