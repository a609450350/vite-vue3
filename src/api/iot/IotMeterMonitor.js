import { httpGet, httpPost } from '@/api/index';

export function iotMonitorList(page, limit, query) {
  return httpGet('iotsyn/iotCollectionRecords/getIotMeterList', {
    page,
    limit,
    ...query,
  });
}

export function getIotMeterInstallData(id) {
  return httpGet('iotsyn/iotCollectionRecords/getIotMeterInstallData', {
    id,
  });
}

export function getIotMeterModelById(id) {
  return httpGet('iotsyn/iotCollectionRecords/getIotMeterModelById', {
    id,
  });
}

export function iotCollectionRecordsList(page, limit, query) {
  return httpGet('iotsyn/iotCollectionRecords/getCollectionRecordsByMeterId', {
    page,
    limit,
    ...query,
  });
}

export function iotCollectionHistoryRecordsList(page, limit, query) {
  return httpGet('iotsyn/iotCollectionRecords/getHistoryCollectionRecordsByMeterId', {
    page,
    limit,
    ...query,
  });
}

export function getIotSysConfDicAll() {
  return httpGet('iotsyn/iotOptDicLog/getIotSysConfDicAll', {
    webOpt: 1,
  });
}

export function addOptMter(form) {
  return httpPost('iotsyn/iotOptDicLog/addOptMter', form);
}

export function batchOptMterOpen(form) {
  return httpPost('iotsyn/iotOptDicLog/batchOptMterOpen', form);
}

export function batchOptMterClose(form) {
  return httpPost('iotsyn/iotOptDicLog/batchOptMterClose', form);
}

export function batchOptMterCoerceClose(form) {
  return httpPost('iotsyn/iotOptDicLog/batchOptMterCoerceClose', form);
}

export function getValveOptLog(page, limit, query) {
  return httpGet('iotsyn/iotOptDicLog/getValveOptLog', {
    page,
    limit,
    ...query,
  });
}

export function iotOptDicLogList(page, limit, query) {
  return httpGet('iotsyn/iotOptDicLog/page', {
    page,
    limit,
    ...query,
  });
}

export function iotOptDicHistoryLogList(page, limit, query) {
  return httpGet('iotsyn/iotOptDicLog/getHistorylist', {
    page,
    limit,
    ...query,
  });
}

export function getIotMeterValveList(page, limit, query) {
  return httpGet('iotsyn/iotOptDicLog/getValveOptLog', {
    page,
    limit,
    ...query,
  });
}

export function getDayMeterStateCount(form) {
  return httpGet('biz/custDayBill/getDayMeterStateCount', form);
}

export function getDayMeterMonthCount(form) {
  return httpGet('biz/custDayBill/getDayMeterMonthCount', form);
}
