import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function meterAlarmInfoList(page, limit, query) {
  return httpGet('iotsyn/meterAlarmInfo/page', {
    page,
    limit,
    ...query,
  });
}

export function meterAlarmInfoAdd(form) {
  return httpPost('iotsyn/meterAlarmInfo', form);
}

export function meterAlarmInfoEdit(form) {
  return httpPut('iotsyn/meterAlarmInfo', form);
}

export function meterAlarmInfoDelete(id) {
  return httpDelete(`iotsyn/meterAlarmInfo/${id}`);
}

export function processAlarmInfo(form) {
  return httpPost('iotsyn/meterAlarmInfo/processAlarmInfo', form);
}
