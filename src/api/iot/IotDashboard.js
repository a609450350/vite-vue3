import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function getNetMeterCount001() {
  return httpGet('biz/meter/getNetMeterCount001');
}
