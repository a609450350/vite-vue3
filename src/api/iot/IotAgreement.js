import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function iotAgreementList(page, limit, query) {
  return httpGet('iotsyn/iotQuery/getIotAgreementList', {
    page,
    limit,
    ...query,
  });
}

export function iotAgreementAdd(form) {
  return httpPost('biz/iotAgreement', form);
}

export function iotAgreementEdit(form) {
  return httpPut('biz/iotAgreement', form);
}

export function iotAgreementDelete(id) {
  return httpDelete(`biz/iotAgreement/${id}`);
}
