import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function groupAllList() {
  return httpGet('zoyoact/processDef/page');
}

export function addGroup(form) {
  return httpPost('zoyoact/group', form);
}

export function editGroup(form) {
  return httpPut('zoyoact/group', form);
}

export function deleteGroup(id) {
  return httpDelete(`zoyoact/group/${id}`);
}

// 流程

export function workflowAllList() {
  return httpGet('zoyoact/processDef/all');
}

export function workflowHistoryList(page = 1, limit = 10, defId = '') {
  return httpGet('publish/page', {
    page,
    limit,
    defId,
  });
}

export function workflowTaskList(page = 1, limit = 10) {
  return httpGet('zoyoact/runJob/getJobList', {
    page,
    limit,
  });
}

export function workflowJobList(page = 1, limit = 10) {
  return httpGet('zoyoact/runJob/getTaskList', {
    page,
    limit,
  });
}

export function getWorkNode(publishId) {
  return httpGet(`zoyoact/runJob/getWorkNode/${publishId}`);
}

export function jumpNode(jobId, targetNode, conment) {
  return httpPost(`zoyoact/runJob/turnTransition/${jobId}/${targetNode}`, {
    conment,
  });
}

export function transferAssignee(form) {
  return httpPost('zoyoact/runJob/transferAssignee', form);
}

export function addWorkflow(form) {
  return httpPost('zoyoact/processDef', form);
}

export function editWorkflow(form) {
  return httpPut('zoyoact/processDef', form);
}

export function publishWorkflow(defId) {
  return httpPost(`zoyoact/publish/publishData/${defId}`);
}

export function moveWorkflow(form) {
  return httpPost('zoyoact/processDef/mobileGroup', form);
}

export function deleteWorkflow(id) {
  return httpDelete(`zoyoact/processDef/${id}`);
}
