import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function conditionsList(page, limit, query) {
  return httpGet('zoyoact/conditions/page', {
    page,
    limit,
    ...query,
  });
}

export function conditionsListByDefId(defId) {
  return httpGet(`zoyoact/conditions/getConditionsByDefId/${defId}`);
}

export function addConditions(form) {
  return httpPost('zoyoact/conditions', form);
}

export function editConditions(form) {
  return httpPut('zoyoact/conditions', form);
}

export function deleteConditions(id) {
  return httpDelete(`zoyoact/conditions/${id}`);
}
