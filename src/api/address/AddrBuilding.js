import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function addrBuildingList(page, limit, query) {
  return httpGet('biz/addrBuilding/page', {
    page,
    limit,
    ...query,
  });
}

export function addrBuildingAdd(form) {
  return httpPost('biz/addrBuilding', form);
}

export function batchAdd(form) {
  return httpPost('biz/addrBuilding/batchAdd', form);
}

export function batchAddrInfo(form) {
  return httpPost('biz/addrBuilding/batchAddrInfo', form);
}

export function addrBuildingEdit(form) {
  return httpPut('biz/addrBuilding', form);
}

export function addrBuildingDelete(id) {
  return httpDelete(`biz/addrBuilding/${id}`);
}
