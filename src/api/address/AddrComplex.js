import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function addrComplexList(page, limit, query) {
  return httpGet('biz/addrComplex/page', {
    page,
    limit,
    ...query,
  });
}

export function addrComplexListAll() {
  return httpGet('biz/addrComplex/all');
}

export function addrComplexAdd(form) {
  return httpPost('biz/addrComplex', form);
}

export function addrComplexEdit(form) {
  return httpPut('biz/addrComplex', form);
}

export function addrComplexDelete(id) {
  return httpDelete(`biz/addrComplex/${id}`);
}
