import { httpDelete, httpGet, httpPost, httpPostForm, httpPut } from '@/api/index';

export function addrInfoList(page, limit, query) {
  return httpGet('biz/addrInfo/page', {
    page,
    limit,
    ...query,
  });
}

export function addrInfoAdd(form) {
  return httpPost('biz/addrInfo', form);
}

export function addrInfoEdit(form) {
  return httpPut('biz/addrInfo', form);
}

export function addrInfoDelete(id) {
  return httpDelete(`biz/addrInfo/${id}`);
}

export function updateAddrBiz(form) {
  return httpPost('biz/addrInfo/updateAddrBiz', form);
}
// 模版导出
export function addrInfoExport(query) {
  return httpGet('biz/addrInfo/getAddrExportTemp', query);
}
// 文件导入
export function addrInfoImport(query) {
  return httpPostForm('biz/addrInfo/addAddrImportTemp', query);
}
// 文件提交
export function addBatchMeterChange(form) {
  return httpPost('biz/addrInfo/addAddrImportSubmit', form);
}
