import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function addrGasAreaList(page, limit, query) {
  return httpGet('biz/addrGasArea/page', {
    page,
    limit,
    ...query,
  });
}

export function addrGasAreaListAll() {
  return httpGet('biz/addrGasArea/all');
}

export function addrGasAreaAdd(form) {
  return httpPost('biz/addrGasArea', form);
}

export function addrGasAreaEdit(form) {
  return httpPut('biz/addrGasArea', form);
}

export function addrGasAreaDelete(id) {
  return httpDelete(`biz/addrGasArea/${id}`);
}
