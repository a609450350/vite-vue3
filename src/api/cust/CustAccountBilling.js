import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function custAccountBillingList(page, limit, query) {
  return httpGet('biz/custAccountBilling/page', {
    page,
    limit,
    ...query,
  });
}

export function custAccountBillingAdd(form) {
  return httpPost('biz/custAccountBilling', form);
}

export function custAccountBillingEdit(form) {
  return httpPut('biz/custAccountBilling', form);
}

export function custAccountBillingDelete(id) {
  return httpDelete(`biz/custAccountBilling/${id}`);
}
