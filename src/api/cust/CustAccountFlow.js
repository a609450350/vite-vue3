import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function custAccountFlowList(page, limit, query) {
  return httpGet('biz/custAccountFlow/page', {
    page,
    limit,
    ...query,
    type: 0,
  });
}

export function custAccountFlowAdd(form) {
  return httpPost('biz/custAccountFlow', form);
}

export function custAccountFlowEdit(form) {
  return httpPut('biz/custAccountFlow', form);
}

export function custAccountFlowDelete(id) {
  return httpDelete(`biz/custAccountFlow/${id}`);
}
