import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function custUserList(page, limit, query) {
  return httpGet('biz/custUser/page', {
    page,
    limit,
    ...query,
  });
}

export function custUserAdd(form) {
  return httpPost('biz/custUser', form);
}

export function custUserEdit(form) {
  return httpPut('biz/custUser', form);
}

export function custUserDelete(id) {
  return httpDelete(`biz/custUser/${id}`);
}

export function custGraph(id) {
  return httpGet(`biz/custUser/getTopologicalGraphCustId`, { id });
}

export function custGraphNode(id, type) {
  return httpGet(`biz/custUser/getTopologicalGraphDetails`, { id, type });
}

export function custRelationUserList(page, limit, query) {
  return httpGet('biz/custUser/getCustRelation', {
    page,
    limit,
    ...query,
  });
}

export function addCustRelation(form) {
  return httpPost('biz/custUser/addCustRelation', form);
}

export function deleteCustRelation(id) {
  return httpPost('biz/custUser/deleteCustRelation', { id });
}

export function getNoCustRelation(page, limit, query) {
  return httpGet('biz/custUser/getNoCustRelation', {
    page,
    limit,
    ...query,
  });
}
