import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function custIdentityList(page, limit, query) {
  return httpGet('biz/custIdentity/page', {
    page,
    limit,
    ...query,
  });
}

export function custIdentityAdd(form) {
  return httpPost('biz/custIdentity', form);
}

export function custIdentityEdit(form) {
  return httpPut('biz/custIdentity', form);
}

export function custIdentityDelete(id) {
  return httpDelete(`biz/custIdentity/${id}`);
}
