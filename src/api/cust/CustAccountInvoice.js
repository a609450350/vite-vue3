import { httpDelete, httpGet, httpPost, httpPut } from '@/api/index';

export function custAccountInvoiceList(page, limit, query) {
  return httpGet('biz/custAccountInvoice/page', {
    page,
    limit,
    ...query,
    type: 1,
  });
}

export function custAccountInvoiceAdd(form) {
  return httpPost('biz/custAccountInvoice', form);
}

export function custAccountInvoiceEdit(form) {
  return httpPut('biz/custAccountInvoice', form);
}

export function custAccountInvoiceDelete(id) {
  return httpDelete(`biz/custAccountInvoice/${id}`);
}
