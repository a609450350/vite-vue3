export default function useModel() {
  const model = () => ({
    name: '',
    provinceId: '',
    cityId: '',
    countyId: '',
    townId: '',
    communityId: '',
    longitude: '',
    latitude: '',
    addr: '',
    num: '',
    areaId: '',
    stationId: '',
  });

  const rules = reactive({
    name: [{ required: true, message: '请输入社区名称', trigger: 'change' }],
    townCode: [{ required: true, message: '请选择乡镇街道', trigger: 'change' }],
    areaCode: [{ required: true, message: '请选择地区', trigger: 'change' }],
    provinceId: [{ required: true, message: '请输入省', trigger: 'change' }],
    cityId: [{ required: true, message: '请输入市', trigger: 'change' }],
    countyId: [{ required: true, message: '请输入区', trigger: 'change' }],
    longitude: [{ required: true, message: '请输入经度', trigger: 'change' }],
    latitude: [{ required: true, message: '请输入纬度', trigger: 'change' }],
    addr: [{ required: true, message: '请输入地理位置', trigger: 'change' }],
    stationId: [{ required: true, message: '请选择管理站', trigger: 'change' }],
  });
  return { model, rules };
}
