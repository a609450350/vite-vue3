import { ElMessage, ElMessageBox } from 'element-plus';

export default function useTable(tableDataFunc, delFunc, queryParams = {}, fixedParams = {}) {
  const queryRef = ref(null);
  const query = reactive({
    isFilter: false,
    data: queryParams,
  });

  const tableInfo = reactive({
    page: 1,
    limit: 10,
    total: 0,
    data: [],
    isLoading: false,
  });

  const getTableInfo = () => {
    tableInfo.isLoading = true;
    let params = fixedParams;
    if (query.isFilter) {
      params = { ...params, ...query.data };
    }
    tableDataFunc(tableInfo.page, tableInfo.limit, params).then(
      (res) => {
        tableInfo.data = res.rows;
        tableInfo.total = res.total;
        tableInfo.isLoading = false;
      },
      () => {
        tableInfo.isLoading = false;
      }
    );
  };

  const goQuery = () => {
    tableInfo.page = 1;
    query.isFilter = true;
    getTableInfo();
  };

  const resetQuery = () => {
    query.isFilter = false;
    queryRef.value.resetFields();
    query.data.startTime = '';
    query.data.endTime = '';
    getTableInfo();
  };

  const switchPage = (e) => {
    tableInfo.page = e;
    getTableInfo();
  };

  const handleSizeChange = (e) => {
    tableInfo.page = 1;
    tableInfo.limit = e;
    getTableInfo();
  };

  const handleDelete = (e) => {
    ElMessageBox.confirm('是否删除?', '提示', {
      confirmButtonText: '确认',
      cancelButtonText: '取消',
      type: 'warning',
    }).then(() => {
      delFunc(e).then(() => {
        ElMessage({
          type: 'success',
          message: '删除成功',
        });
        if (tableInfo.data.length <= 1 && tableInfo.page >= 2) {
          tableInfo.page -= 1;
        }
        getTableInfo();
      });
    });
  };

  return {
    queryRef,
    query,
    goQuery,
    resetQuery,
    tableInfo,
    getTableInfo,
    switchPage,
    handleSizeChange,
    handleDelete,
  };
}
