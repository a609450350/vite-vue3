import { ElMessage } from 'element-plus';

export default function useForm(model, refreshFunc) {
  const dialog = reactive({
    show: false,
    isEdit: false,
    isLoading: false,
  });

  const formRef = ref(null);
  const dataForm = ref(model());

  const submitForm = (submitMethod) => {
    formRef.value.validate((valid) => {
      if (valid) {
        dialog.isLoading = true;
        submitMethod(dataForm.value).then(
          () => {
            dialog.isLoading = false;
            ElMessage({
              type: 'success',
              message: '操作成功',
            });
            dialog.show = false;
            refreshFunc();
          },
          () => {
            dialog.isLoading = false;
          }
        );
      }
    });
  };

  const resetForm = () => {
    formRef.value.resetFields();
  };

  return {
    formRef,
    dataForm,
    dialog,
    resetForm,
    submitForm,
  };
}
