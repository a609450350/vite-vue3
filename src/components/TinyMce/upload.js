import { httpImg } from '@/api/index';

import Minio from '@/utils/minio';

export default async function TinyMCEUploadHandler(blobInfo, progress) {
  return new Promise((resolve, reject) => {
    Minio.getInstance()
      .upload2(blobInfo.blob())
      .then((res) => {
        resolve(httpImg(res.url));
      });
  });
}
