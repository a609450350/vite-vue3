import { store } from "@/store";
import { defineStore } from "pinia";
import { cloneDeep } from "lodash-es";
import BizRoutes from "@/router/routes/biz";
import ConstantRoutes from "@/router/routes/constant";
export const userAppStore = defineStore("app-user", {
  state: () => ({
    // user info
    username: "",
    password: "",
    userInfo: "",
    // token
    token: "",
    fiToken: "",
    tentId: "",
    dictionaries: [],
    dictionaryTree: [],
    elements: [],
    userCompany: "",
    routes: [],
  }),
  actions: {
    generateRoutes() {
      const bizNewRoute = removeComponent(cloneDeep(BizRoutes));

      const mergeRoutes = [
        ...ConstantRoutes,
        ...bizNewRoute,
        {
          path: `/:pathMatch(.*)*`,
          redirect: { name: "404" },
          hidden: true,
        },
      ];
      this.routes = mergeRoutes;
    },
  },
});
const removeComponent = (routerMap, parent) => {
  return routerMap.map((item) => {
    item.component = "";
    if (item.children && item.children.length > 0) {
      item.children = removeComponent(item.children, item);
    }
    return item;
  });
};
export function useUserStoreWithOut() {
  return userAppStore(store);
}
