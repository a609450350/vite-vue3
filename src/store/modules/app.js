import { store } from "@/store";
import { defineStore } from "pinia";
export const useAppStore = defineStore("app", {
  state: () => ({
    id: "app",
    sidebarOpen: false,
    fullWindow: false,
    routeLoading: false, //首屏加载
    lang: "zhCN",
    token: undefined,
    visitedViews: [],
    cachedViews: [],
    device: "desktop",
    lang: "zhCN",
    isLock: false,
    lockPassword: "",
    enabledCachedViews:
      localStorage.getItem("enabledCachedViews") === null
        ? false
        : localStorage.getItem("enabledCachedViews") !== "false",
  }),
  actions: {
    closeSidebar() {
      this.sidebarOpen = false;
    },
    setCachedState(e) {
      this.enabledCachedViews = e;
      localStorage.setItem("enabledCachedViews", e);
      window.location.reload();
    },
    toggleDevice(device) {
      this.device = device;
    },
    toggleSidebar() {
      this.sidebarOpen = !this.sidebarOpen;
    },
    addView(view) {
      this.addVisitedView(view);
      this.addCachedView(view);
    },
    addVisitedView(view) {
      if (this.visitedViews.some((v) => v.path === view.path)) return;
      this.visitedViews.push({ ...view, title: view.meta.title || "no-name" });
    },
    addCachedView(view) {
      if (this.cachedViews.includes(view.name)) return;
      if (!view.meta.noCache) {
        this.cachedViews.push(view.name);
      }
    },
    delView(view) {
      this.delVisitedView(view);
      this.delCachedView(view);
    },
    delVisitedView(view) {
      const i = this.visitedViews.findIndex((v) => v.path === view.path);
      if (i !== -1) {
        this.visitedViews.splice(i, 1);
      }
    },
    delCachedView(view) {
      const i = this.cachedViews.findIndex((v) => v.path === view.path);
      if (i !== -1) {
        this.cachedViews.splice(i, 1);
      }
    },
    delLeftVisitedView(view) {
      const i = this.visitedViews.findIndex((v) => v.path === view.path);
      if (i !== -1) {
        this.visitedViews.splice(0, i);
      }
    },
    delLeftCachedView(view) {
      const i = this.cachedViews.findIndex((v) => v.path === view.path);
      if (i !== -1) {
        this.cachedViews.splice(0, i);
      }
    },
    delLeftView(view) {
      this.delLeftVisitedView(view);
      this.delLeftCachedView(view);
    },
    delRightVisitedView(view) {
      const i = this.visitedViews.findIndex((v) => v.path === view.path);
      if (i !== -1) {
        this.visitedViews.splice(i + 1, this.visitedViews.length);
      }
    },
    delRightCachedView(view) {
      const i = this.cachedViews.findIndex((v) => v.path === view.path);
      if (i !== -1) {
        this.cachedViews.splice(i + 1, this.cachedViews.length);
      }
    },
    delRightView(view) {
      this.delRightCachedView(view);
      this.delRightVisitedView(view);
    },
    delOtherView(view) {
      this.delOtherVisitedView({ ...view });
      this.delOtherCachedView({ ...view });
    },
    delOtherVisitedView(view) {
      const i = this.visitedViews.findIndex((v) => v.path === view.path);
      if (i !== -1) {
        this.visitedViews = [this.visitedViews[i]];
      }
    },
    delOtherCachedView(view) {
      const i = this.cachedViews.findIndex((v) => v.path === view.path);
      if (i !== -1) {
        this.cachedViews = [this.cachedViews[i]];
      }
    },
    delAllView() {
      this.delAllVisitedView();
      this.delAllCachedView();
    },
    delAllVisitedView() {
      this.visitedViews = [];
    },
    delAllCachedView() {
      this.cachedViews = [];
    },
  },
});

export function useAppStoreWithOut() {
  return useAppStore(store);
}
