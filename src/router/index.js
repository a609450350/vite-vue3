import { createRouter, createWebHistory } from "vue-router";

import bizRoutes from "./routes/biz";
import constantRoutes from "./routes/constant";
const routes = [
  ...constantRoutes,
  ...bizRoutes,
  // {
  //   path: "/",
  //   name: "Home",
  //   component: () => import("@/views/sys/auth/login.vue"),
  //   hidden: true,
  // },
];

const router = createRouter({
  history: createWebHistory(),
  scrollBehavior: () => ({ y: 0 }),
  routes,
});
export default router;
