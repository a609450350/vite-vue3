import NProgress from "nprogress";
import "nprogress/nprogress.css";

import whiteList from "./routes/white-list";
import { useAppStoreWithOut } from "@/store/modules/app";
import { useUserStoreWithOut } from "@/store/modules/user";
const appStore = useAppStoreWithOut();
const userStore = useUserStoreWithOut();

import router from "./index";

NProgress.configure({ showSpinner: false });

router.beforeEach((to, from, next) => {
  appStore.routeLoading = true;
  NProgress.start();
  if (whiteList.indexOf(to.path) !== -1) {
    console.log(whiteList.indexOf(to.path) !== -1);
    next();
  } else if (sessionStorage.getItem("token")) {
    userStore.generateRoutes();
    if (to.path === "/login") {
      next({ path: "/" });
      NProgress.done();
    } else {
      next();
    }
  } else {
    next({ path: "/login" });
    NProgress.done();
  }
});

router.afterEach((to, from, next) => {
  appStore.routeLoading = false;
  NProgress.done();
});
