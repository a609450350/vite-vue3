import LayoutDefault from "../../layout/default/index.vue";
import FIDefault from "../../layout/fi/index.vue";

const env = "test";
const getLayoutComponent = () => {
  if (env === "preview") {
    return FIDefault;
  }
  return LayoutDefault;
};
const route = [
  {
    path: "/address",
    name: "/address",
    component: getLayoutComponent(),
    meta: {
      title: "地址管理",
      icon: "Antd-EnvironmentTwotone",
      noCache: false,
      type: "dirt",
      href: "/address",
      code: "Layout",
    },
    hideChildrenInMenu: true,
    children: [
      {
        path: "/address/community",
        name: "/address/community",
        component: () =>
          import(
            "@/views/customer-service/address/AddrComplex/AddrComplex.vue"
          ),
        meta: {
          title: "小区管理",
          noCache: false,
          type: "menu",
          href: "/address/community",
          code: "AddressCommunity",
        },
        hideChildrenInMenu: true,
      },
    ],
    //   {
    //     path: "/address/building",
    //     name: "/address/building",
    //     component: () =>
    //       import(
    //         "@/views/customer-service/address/AddrBuilding/AddrBuilding.vue"
    //       ),
    //     meta: {
    //       title: "楼栋管理",
    //       noCache: false,
    //       type: "menu",
    //       href: "/address/building",
    //       code: "AddressBuilding",
    //     },
    //     hideChildrenInMenu: true,
    //   },
    //   {
    //     path: "/address/point",
    //     name: "/address/point",
    //     component: () =>
    //       import("@/views/customer-service/address/AddrInfo/AddrInfo.vue"),
    //     meta: {
    //       title: "服务地址",
    //       noCache: false,
    //       type: "menu",
    //       href: "/address/point",
    //       code: "AddressPoint",
    //     },
    //     hideChildrenInMenu: true,
    //   },
    // ],
  },
];
export default route;
