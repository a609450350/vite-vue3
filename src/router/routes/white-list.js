const whiteList = [
  "/login",
  "/reset",
  "/register",
  "/404",
  "/403",
  "/500",
  "/502",
  "/503",
];
export default whiteList;
