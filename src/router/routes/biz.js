import LayoutDefault from "@/layout/default/index.vue";
import FIDefault from "@/layout/fi/index.vue";

// const env = import.meta.env.VITE_APP_ENV;
const env = "test";

const getLayoutComponent = () => {
  if (env === "preview") {
    return FIDefault;
  }
  return LayoutDefault;
};

const route = [
  {
    name: "CeOuterView",
    path: "/query/cust-outer-view",
    component: () =>
      import("@/views/customer-service/query/AddrQuery/CeOuterView.vue"),
    hidden: true,
  },
  {
    path: "/address",
    name: "/address",
    component: getLayoutComponent(),
    meta: {
      title: "地址管理",
      icon: "Antd-EnvironmentTwotone",
      noCache: false,
      type: "dirt",
      href: "/address",
      code: "Layout",
    },
    hideChildrenInMenu: true,
    children: [
      {
        path: "/address/community",
        name: "/address/community",
        component: () =>
          import(
            "@/views/customer-service/address/AddrComplex/AddrComplex.vue"
          ),
        meta: {
          title: "小区管理",
          noCache: false,
          type: "menu",
          href: "/address/community",
          code: "AddressCommunity",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/address/building",
        name: "/address/building",
        component: () =>
          import(
            "@/views/customer-service/address/AddrBuilding/AddrBuilding.vue"
          ),
        meta: {
          title: "楼栋管理",
          noCache: false,
          type: "menu",
          href: "/address/building",
          code: "AddressBuilding",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/address/point",
        name: "/address/point",
        component: () =>
          import("@/views/customer-service/address/AddrInfo/AddrInfo.vue"),
        meta: {
          title: "服务地址",
          noCache: false,
          type: "menu",
          href: "/address/point",
          code: "AddressPoint",
        },
        hideChildrenInMenu: true,
      },
    ],
  },
  {
    path: "/product",
    name: "/product",
    component: getLayoutComponent(),
    meta: {
      title: "产品管理",
      icon: "Antd-ShoppingOutlined",
      noCache: false,
      type: "dirt",
      href: "/product",
      code: "Layout",
    },
    hideChildrenInMenu: true,
    children: [
      {
        path: "/product/info",
        name: "/product/info",
        component: () =>
          import(
            "@/views/customer-service/product/ProductInfo/ProductInfo.vue"
          ),
        meta: {
          title: "产品信息",
          noCache: false,
          type: "menu",
          href: "/product/info",
          code: "CustomerProduct",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/product/package",
        name: "/product/package",
        component: () =>
          import(
            "@/views/customer-service/product/ProductPackage/ProductPackage.vue"
          ),
        meta: {
          title: "产品套餐",
          noCache: false,
          type: "menu",
          href: "/product/package",
          code: "ProductPackage",
        },
        hideChildrenInMenu: true,
      },
    ],
  },
  {
    path: "/biz",
    name: "/biz",
    component: getLayoutComponent(),
    meta: {
      title: "业务办理",
      icon: "Antd-AppstoreOutlined",
      noCache: false,
      type: "dirt",
      href: "/biz",
      code: "Layout",
    },

    hideChildrenInMenu: true,
    children: [
      {
        path: "/biz/comprehensive-business-deal",
        name: "/biz/comprehensive-business-deal",
        component: () =>
          import(
            "@/views/customer-service/business/ComprehensiveBusinessDeal.vue"
          ),
        meta: {
          title: "综合业务办理",
          noCache: false,
          type: "menu",
          href: "/biz/comprehensive-business-deal",
          code: "ComprehensiveBusinessDeal",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/charge/card-charge",
        name: "/charge/card-charge",
        component: () =>
          import(
            "@/views/customer-service/charge/ICCardCharge/ICCardCharge.vue"
          ),
        meta: {
          title: "卡表业务办理",
          noCache: false,
          type: "menu",
          href: "/charge/card-charge",
          code: "CardCharge",
        },
        hideChildrenInMenu: true,
      },

      {
        path: "/business/subsidy-apply",
        name: "/business/subsidy-apply",
        component: () =>
          import(
            "@/views/customer-service/business/ApprovalInfo/ApprovalInfo.vue"
          ),
        meta: {
          title: "特批申请",
          noCache: false,
          type: "menu",
          href: "/system/subsidy-apply",
          code: "ApprovalInfo",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/business/subsidy-approval",
        name: "/business/subsidy-approval",
        component: () =>
          import(
            "@/views/customer-service/business/SubsidyApproval/SubsidyApproval.vue"
          ),
        meta: {
          title: "特批审批",
          noCache: false,
          type: "menu",
          href: "/system/subsidy-approval",
          code: "SubsidyApproval",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/system/subsidy",
        name: "/system/subsidy",
        component: () =>
          import("@/views/system/SysConfSubsidy/SysConfSubsidy.vue"),
        meta: {
          title: "特批配置",
          noCache: false,
          type: "menu",
          href: "/system/subsidy",
          code: "SysConfSubsidy",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/business/preference-subsidy",
        name: "/business/preference-subsidy",
        component: () =>
          import(
            "@/views/customer-service/business/PreferenceSubsidy/PreferenceSubsidy.vue"
          ),
        meta: {
          title: "优惠补贴",
          noCache: false,
          type: "menu",
          href: "/system/preference-subsidy",
          code: "PreferenceSubsidy",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/biz/batch-open-account",
        name: "/biz/batch-open-account",
        component: () =>
          import(
            "@/views/customer-service/business/batch/BatchOpenAccount/BatchOpenAccount.vue"
          ),
        meta: {
          title: "批量开户",
          noCache: false,
          type: "menu",
          href: "/biz/batch-open-account",
          code: "BatchOpenAccount",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/biz/batch-fire",
        name: "/biz/batch-fire",
        component: () =>
          import(
            "@/views/customer-service/business/batch/BatchFire/BatchFire.vue"
          ),
        meta: {
          title: "批量开通",
          noCache: false,
          type: "menu",
          href: "/biz/batch-fire",
          code: "BatchFire",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/biz/batch-install-meter",
        name: "/biz/batch-install-meter",
        component: () =>
          import(
            "@/views/customer-service/business/batch/BatchInstallMeter/BatchInstallMeter.vue"
          ),
        meta: {
          title: "批量装表",
          noCache: false,
          type: "menu",
          href: "/biz/batch-install-meter",
          code: "BatchInstallMeter",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/biz/batch-change-meter",
        name: "/biz/batch-change-meter",
        component: () =>
          import(
            "@/views/customer-service/business/batch/BatchChangeMeter/BatchChangeMeter.vue"
          ),
        meta: {
          title: "批量换表",
          noCache: false,
          type: "menu",
          href: "/biz/batch-change-meter",
          code: "BatchChangeMeter",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/biz/batch-product-change",
        name: "/biz/batch-product-change",
        component: () =>
          import(
            "@/views/customer-service/business/batch/BatchProductChange/BatchProductChange.vue"
          ),
        meta: {
          title: "批量变价",
          noCache: false,
          type: "menu",
          href: "/biz/batch-product-change",
          code: "BatchProductChange",
        },
        hideChildrenInMenu: true,
      },
    ],
  },
  {
    path: "/query",
    name: "/query",
    component: getLayoutComponent(),
    meta: {
      title: "业务查询",
      icon: "Antd-SearchOutlined",
      noCache: false,
      type: "dirt",
      href: "/query",
      code: "Layout",
    },
    hideChildrenInMenu: true,
    children: [
      {
        path: "/query/comprehensive-business-deal-query",
        name: "/query/comprehensive-business-deal-query",
        component: () =>
          import("@/views/customer-service/query/BissQuery/BissQuery.vue"),
        meta: {
          title: "业务办理查询",
          noCache: false,
          type: "menu",
          href: "/query/comprehensive-business-deal-query",
          code: "ComprehensiveBusinessDealQuery",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/query/customer",
        name: "/query/customer",
        component: () =>
          import("@/views/customer-service/query/CustQuery/CustQuery.vue"),
        meta: {
          title: "客户查询",
          noCache: false,
          type: "menu",
          href: "/query/customer",
          code: "CustomerQuery",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/query/address",
        name: "/query/address",
        component: () =>
          import("@/views/customer-service/query/AddrQuery/AddrQuery.vue"),
        meta: {
          title: "360视图",
          noCache: false,
          type: "menu",
          href: "/query/address",
          code: "AddressQuery",
        },
        hideChildrenInMenu: true,
      },

      {
        path: "/query/receivable",
        name: "/query/receivable",
        component: () =>
          import(
            "@/views/customer-service/query/ReceivableQuery/ReceivableQuery.vue"
          ),
        meta: {
          title: "应收查询",
          noCache: false,
          type: "menu",
          href: "/query/receivable",
          code: "ReceivableQuery",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/query/net-receipts",
        name: "/query/net-receipts",
        component: () =>
          import(
            "@/views/customer-service/query/NetReceiptsQuery/NetReceiptsQuery.vue"
          ),
        meta: {
          title: "实收查询",
          noCache: false,
          type: "menu",
          href: "/query/net-receipts",
          code: "NetReceiptsQuery",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/query/cust-arrearage-bill",
        name: "/query/cust-arrearage-bill",
        component: () =>
          import(
            "@/views/customer-service/query/CustArrearageBill/CustArrearageBill.vue"
          ),
        meta: {
          title: "欠费查询",
          noCache: false,
          type: "menu",
          href: "/query/cust-arrearage-bill",
          code: "CustArrearageBill",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/meter/smart-table",
        name: "/meter/smart-table",
        component: () =>
          import(
            "@/views/customer-service/query/SmartTableQuery/SmartTableQuery.vue"
          ),
        meta: {
          title: "智能表查询",
          noCache: false,
          type: "menu",
          href: "/meter/smart-table",
          code: "SmartTable",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/meter/meter-select",
        name: "/meter/meter-select",
        component: () =>
          import(
            "@/views/customer-service/query/MeterInstallQuery/MeterInstallQuery.vue"
          ),
        meta: {
          title: "安装表具查询",
          noCache: false,
          type: "menu",
          href: "/meter/meter-select",
          code: "MeterInstallQuery",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/charge/pay-log",
        name: "/charge/pay-log",
        component: () =>
          import("@/views/customer-service/charge/PayLog/PayLog.vue"),
        meta: {
          title: "聚合支付记录",
          noCache: false,
          type: "menu",
          href: "/charge/pay-log",
          code: "PayLog",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/charge/net-pay-log",
        name: "/charge/net-pay-log",
        component: () =>
          import("@/views/customer-service/charge/NetPayLog/NetPayLog.vue"),
        meta: {
          title: "网厅支付记录",
          noCache: false,
          type: "menu",
          href: "/charge/net-pay-log",
          code: "NetPayLog",
        },
        hideChildrenInMenu: true,
      },
    ],
  },
  {
    path: "/customer/meter",
    name: "/customer/meter",
    component: getLayoutComponent(),
    meta: {
      title: "表具管理",
      icon: "Antd-ControlTwotone",
      noCache: false,
      type: "dirt",
      href: "/customer/meter",
      code: "Layout",
    },
    hideChildrenInMenu: true,
    children: [
      {
        path: "/meter/info",
        name: "/meter/info",
        component: () =>
          import("@/views/customer-service/meter/Meter/Meter.vue"),
        meta: {
          title: "表具信息",
          noCache: false,
          type: "menu",
          href: "/meter/info",
          code: "MeterInfo",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/meter/spec",
        name: "/meter/spec",
        component: () =>
          import("@/views/customer-service/meter/MeterModel/MeterModel.vue"),
        meta: {
          title: "表具规格",
          noCache: false,
          type: "menu",
          href: "/meter/spec",
          code: "MeterSpec",
        },
        hideChildrenInMenu: true,
      },
      // {
      //   path: '/meter/devices',
      //   name: '/meter/devices',
      //   component: () => import('@/views/customer-service/meter/GaRecord/GaRecord.vue'),
      //   meta: {
      //     title: '燃气设备',
      //     noCache: false,
      //     type: 'menu',
      //     href: '/meter/devices',
      //     code: 'MeterDevices',
      //   },
      //   hideChildrenInMenu: true,
      // },
    ],
  },
  {
    path: "/customer",
    name: "/customer",
    component: getLayoutComponent(),
    meta: {
      title: "客户管理",
      icon: "Antd-CrownTwotone",
      noCache: false,
      type: "dirt",
      href: "/customer",
      code: "Layout",
    },
    hideChildrenInMenu: true,
    children: [
      {
        path: "/customer/info",
        name: "/customer/info",
        component: () =>
          import("@/views/customer-service/cust/CustUser/CustUser.vue"),
        meta: {
          title: "客户信息",
          noCache: false,
          type: "menu",
          href: "/customer/info",
          code: "CustomerInfo",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/customer/account",
        name: "/customer/account",
        component: () =>
          import("@/views/customer-service/cust/CustIdentity/CustIdentity.vue"),
        meta: {
          title: "账户信息",
          noCache: false,
          type: "menu",
          href: "/customer/account",
          code: "CustomerAccount",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/system/annex-manage",
        name: "/system/annex-manage",
        component: () =>
          import("@/views/customer-service/cust/AnnexManage/AnnexManage.vue"),
        meta: {
          title: "客户附件",
          noCache: false,
          type: "menu",
          href: "/system/annex-manage",
          code: "AnnexManage",
        },
        hideChildrenInMenu: true,
      },
    ],
  },
  {
    path: "/bill",
    name: "/bill",
    component: getLayoutComponent(),
    meta: {
      title: "账单费用",
      icon: "Antd-FileTextTwotone",
      noCache: false,
      type: "dirt",
      href: "/bill",
      code: "Layout",
    },
    hideChildrenInMenu: true,
    children: [
      {
        path: "/bill/query",
        name: "/bill/query",
        component: () =>
          import("@/views/customer-service/bill/CustBill/CustBill.vue"),
        meta: {
          title: "账单查询",
          noCache: false,
          type: "menu",
          href: "/bill/query",
          code: "BillQuery",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/bill/cancel",
        name: "/bill/cancel",
        component: () =>
          import(
            "@/views/customer-service/bill/CustBillCancel/CustBillCancel.vue"
          ),
        meta: {
          title: "账单取消",
          noCache: false,
          type: "menu",
          href: "/bill/cancel",
          code: "BillCancel",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/bill/day",
        name: "/bill/day",
        component: () =>
          import("@/views/customer-service/bill/CustDayBill/CustDayBill.vue"),
        meta: {
          title: "日账单查询",
          noCache: false,
          type: "menu",
          href: "/bill/day",
          code: "CustDayBill",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/bill/material-cost",
        name: "/bill/material-cost",
        component: () =>
          import("@/views/customer-service/bill/CustBillCost/CustBillCost.vue"),
        meta: {
          title: "材料费账单",
          noCache: false,
          type: "menu",
          href: "/bill//material-cost",
          code: "CustBillCost",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/bill/other-cost",
        name: "/bill/other-cost",
        component: () =>
          import("@/views/customer-service/bill/CustBillOth/CustBillOth.vue"),
        meta: {
          title: "其他费账单",
          noCache: false,
          type: "menu",
          href: "/bill/other-cost",
          code: "CustBillOth",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/query/other-fee",
        name: "/query/other-fee",
        component: () =>
          import("@/views/customer-service/query/OtherFee/OtherFee.vue"),
        meta: {
          title: "其他费扣表",
          noCache: false,
          type: "menu",
          href: "/query/other-fee",
          code: "OtherFee",
        },
        hideChildrenInMenu: true,
      },

      {
        path: "/system/fee-maintenance",
        name: "/system/fee-maintenance",
        component: () =>
          import("@/views/customer-service/bill/CustFeeInfo/CustFeeInfo.vue"),
        meta: {
          title: "费用维护",
          noCache: false,
          type: "menu",
          href: "/system/fee-maintenance",
          code: "FeeMaintenance",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/charge/fee-input",
        name: "/charge/fee-input",
        component: () =>
          import(
            "@/views/customer-service/bill/CustBillFeeInfo/CustBillFeeInfo.vue"
          ),
        meta: {
          title: "费用录入",
          noCache: false,
          type: "menu",
          href: "/charge/fee-input",
          code: "FeeInput",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/system/fee-config",
        name: "/system/fee-config",
        component: () =>
          import(
            "@/views/customer-service/bill/ExpenseAllocation/ExpenseAllocation.vue"
          ),
        meta: {
          title: "费用配置",
          noCache: false,
          type: "menu",
          href: "/system/fee-config",
          code: "FeeConfig",
        },
        hideChildrenInMenu: true,
      },
    ],
  },
  {
    path: "/charge",
    name: "/charge",
    component: getLayoutComponent(),
    meta: {
      title: "收费充值",
      icon: "Antd-MoneyCollectTwotone",
      noCache: false,
      type: "dirt",
      href: "/charge",
      code: "Layout",
    },
    hideChildrenInMenu: true,
    children: [
      {
        path: "/charge/counter-charge",
        name: "/charge/counter-charge",
        component: () =>
          import(
            "@/views/customer-service/charge/CounterCharge/CounterCharge.vue"
          ),
        meta: {
          title: "柜台收费",
          noCache: false,
          type: "menu",
          href: "/charge/counter-charge",
          code: "CounterCharge",
        },
        hideChildrenInMenu: true,
      },

      {
        path: "/charge/counter-charge-cancel",
        name: "/charge/counter-charge-cancel",
        component: () =>
          import(
            "@/views/customer-service/charge/CounterChargeCancel/CounterChargeCancel.vue"
          ),
        meta: {
          title: "取消收费",
          noCache: false,
          type: "menu",
          href: "/charge/counter-charge-cancel",
          code: "CounterChargeCancel",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/charge/smart-meter-charge-cancel",
        name: "/charge/smart-meter-charge-cancel",
        component: () =>
          import(
            "@/views/customer-service/charge/SmartMeterCancelCharge/SmartMeterCancelCharge.vue"
          ),
        meta: {
          title: "智能表退费",
          noCache: false,
          type: "menu",
          href: "/charge/smart-meter-charge-cancel",
          code: "SmartMeterCancelCharge",
        },
        hideChildrenInMenu: true,
      },

      {
        path: "/charge/cust-pay-recon",
        name: "/charge/cust-pay-recon",
        component: () =>
          import(
            "@/views/customer-service/charge/CustPayRecon/CustPayRecon.vue"
          ),
        meta: {
          title: "日结记录",
          noCache: false,
          type: "menu",
          href: "/charge/cust-pay-recon",
          code: "CustPayRecon",
        },
        hideChildrenInMenu: true,
      },

      {
        path: "/charge/iot-charge",
        name: "/charge/iot-charge",
        component: () =>
          import("@/views/customer-service/charge/IOTCharge/IOTCharge.vue"),
        meta: {
          title: "远传充值",
          noCache: false,
          type: "menu",
          href: "/charge/iot-charge",
          code: "IotCharge",
        },
        hideChildrenInMenu: true,
      },

      {
        path: "/charge/iot-supply",
        name: "/charge/iot-supply",
        component: () =>
          import("@/views/customer-service/charge/IOTSupply/IOTSupply.vue"),
        meta: {
          title: "远传补量",
          noCache: false,
          type: "menu",
          href: "/charge/iot-supply",
          code: "IOTSupply",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/charge/cancel-supply",
        name: "/charge/cancel-supply",
        component: () =>
          import(
            "@/views/customer-service/charge/CancelSupply/CancelSupply.vue"
          ),
        meta: {
          title: "远传扣量",
          noCache: false,
          type: "menu",
          href: "/charge/cancel-supply",
          code: "CancelSupply",
        },
        hideChildrenInMenu: true,
      },
    ],
  },
  {
    path: "/bill-manage",
    name: "/bill-manage",
    component: getLayoutComponent(),
    meta: {
      title: "发票管理",
      icon: "Antd-ProjectTwotone",
      noCache: false,
      type: "dirt",
      href: "/bill-manage",
      code: "Layout",
    },
    hideChildrenInMenu: true,
    children: [
      {
        path: "/bill-manage/manage",
        name: "/bill-manage/manage",
        component: () =>
          import(
            "@/views/customer-service/bill-manage/RiseManage/RiseManage.vue"
          ),
        meta: {
          title: "抬头管理",
          noCache: false,
          type: "menu",
          href: "/bill-manage/manage",
          code: "RiseManage",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/bill-manage/bill-maintenance",
        name: "/bill-manage/bill-maintenance",
        component: () =>
          import(
            "@/views/customer-service/bill-manage/BillMaintenance/BillMaintenance.vue"
          ),
        meta: {
          title: "税率维护",
          noCache: false,
          type: "menu",
          href: "/bill-manage/bill-maintenance",
          code: "BillMaintenance",
        },
        hideChildrenInMenu: true,
      },
    ],
  },
  {
    path: "/meter-reading",
    name: "/meter-reading",
    component: getLayoutComponent(),
    meta: {
      title: "抄表管理",
      icon: "Antd-SolutionOutlined",
      noCache: false,
      type: "dirt",
      href: "/meter-reading",
      code: "Layout",
    },
    hideChildrenInMenu: true,
    children: [
      {
        path: "/meter-reading/type",
        name: "/meter-reading/type",
        component: () =>
          import(
            "@/views/customer-service/meter-reading/MrRouteType/MrRouteType.vue"
          ),
        meta: {
          title: "抄表类型",
          noCache: false,
          type: "menu",
          href: "/meter-reading/type",
          code: "MeterReadingType",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/meter-reading/task",
        name: "/meter-reading/task",
        component: () =>
          import("@/views/customer-service/meter-reading/MrTask/MrTask.vue"),
        meta: {
          title: "抄表任务",
          noCache: false,
          type: "menu",
          href: "/meter-reading/task",
          code: "MeterReadingTask",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/meter-reading/supplement",
        name: "/meter-reading/supplement",
        component: () =>
          import(
            "@/views/customer-service/meter-reading/MrSupplement/MrSupplement.vue"
          ),
        meta: {
          title: "抄表补抄",
          noCache: false,
          type: "menu",
          href: "/meter-reading/supplement",
          code: "MrSupplement",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/meter-reading/record",
        name: "/meter-reading/record",
        component: () =>
          import(
            "@/views/customer-service/meter-reading/MrRecord/MrRecord.vue"
          ),
        meta: {
          title: "抄表记录",
          noCache: false,
          type: "menu",
          href: "/meter-reading/record",
          code: "MeterReadingRecord",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/meter-reading/plan",
        name: "/meter-reading/plan",
        component: () =>
          import("@/views/customer-service/meter-reading/MrRoute/MrRoute.vue"),
        meta: {
          title: "抄表计划",
          noCache: false,
          type: "menu",
          href: "/meter-reading/plan",
          code: "MeterReadingPlan",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/org/team",
        name: "/org/team",
        component: () =>
          import("@/views/customer-service/org/MrTeam/MrTeam.vue"),
        meta: {
          title: "抄表班组",
          noCache: false,
          type: "menu",
          href: "/org/team",
          code: "MeterReadingTeam",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/meter-reading/plan-input",
        name: "/meter-reading/plan-input",
        component: () =>
          import(
            "@/views/customer-service/meter-reading/MrRouteInput/MrRouteInput.vue"
          ),
        meta: {
          title: "计划录入",
          noCache: false,
          type: "menu",
          href: "/meter-reading/plan-input",
          code: "MrRouteInput",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/meter-reading/batch-input",
        name: "/meter-reading/batch-input",
        component: () =>
          import(
            "@/views/customer-service/meter-reading/MrBatchInput/MrBatchInput.vue"
          ),
        meta: {
          title: "批量录入",
          noCache: false,
          type: "menu",
          href: "/meter-reading/batch-input",
          code: "MeterReadingBatchInput",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/meter-reading/approval",
        name: "/meter-reading/approval",
        component: () =>
          import(
            "@/views/customer-service/meter-reading/MrApproval/MrApproval.vue"
          ),
        meta: {
          title: "抄表审核",
          noCache: false,
          type: "menu",
          href: "/meter-reading/approval",
          code: "MeterReadingApproval",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/meter-reading/approvals",
        name: "/meter-reading/approvals",
        component: () =>
          import(
            "@/views/customer-service/meter-reading/MrApprovals/MrApprovals.vue"
          ),
        meta: {
          title: "抄表计划审核",
          noCache: false,
          type: "menu",
          href: "/meter-reading/approvals",
          code: "MeterReadingApprovals",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/meter-reading/make-bill",
        name: "/meter-reading/make-bill",
        component: () =>
          import(
            "@/views/customer-service/meter-reading/MrRouteBilling/MrRouteBilling.vue"
          ),
        meta: {
          title: "抄表出账",
          noCache: false,
          type: "menu",
          href: "/meter-reading/make-bill",
          code: "MrRouteBilling",
        },
        hideChildrenInMenu: true,
      },
    ],
  },

  // {
  //   path: '/safty-check',
  //   name: '/safty-check',
  //   component: getLayoutComponent(),
  //   meta: {
  //     title: '安检管理',
  //     icon: 'Antd-SafetyCertificateTwotone',
  //     noCache: false,
  //     type: 'dirt',
  //     href: '/safty-check',
  //     code: 'Layout',
  //   },
  //   hideChildrenInMenu: true,
  //   children: [
  //     {
  //       path: '/safty-check/type',
  //       name: '/safty-check/type',
  //       component: () => import('@/views/safty-check/ScRouteType/ScRouteType.vue'),
  //       meta: {
  //         title: '安检类型',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/safty-check/type',
  //         code: 'SaftyCheckType',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/safty-check/item',
  //       name: '/safty-check/item',
  //       component: () => import('@/views/safty-check/ScRouteItem/ScRouteItem.vue'),
  //       meta: {
  //         title: '安检项',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/safty-check/item',
  //         code: 'SaftyCheckItem',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/safty-check/plan',
  //       name: '/safty-check/plan',
  //       component: () => import('@/views/safty-check/ScRoute/ScRoute.vue'),
  //       meta: {
  //         title: '安检计划',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/safty-check/plan',
  //         code: 'SaftyCheckPlan',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/safty-check/task',
  //       name: '/safty-check/task',
  //       component: () => import('@/views/safty-check/ScWorkOrder/ScWorkOrder.vue'),
  //       meta: {
  //         title: '安检任务',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/safty-check/task',
  //         code: 'SaftyCheckTask',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/safty-check/not-joined',
  //       name: '/safty-check/not-joined',
  //       component: () => import('@/views/safty-check/ScNotJoined/ScNotJoined.vue'),
  //       meta: {
  //         title: '未编地址',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/safty-check/not-joined',
  //         code: 'ScNotJoined',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/safty-check/approval',
  //       name: '/safty-check/approval',
  //       component: () => import('@/views/safty-check/ScRecord/ScRecord.vue'),
  //       meta: {
  //         title: '结果审核',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/safty-check/approval',
  //         code: 'ScApproval',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/safty-check/defect-treatment',
  //       name: '/safty-check/defect-treatment',
  //       component: () => import('@/views/safty-check/ScRisk/ScRisk.vue'),
  //       meta: {
  //         title: '隐患处理',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/safty-check/defect-treatment',
  //         code: 'ScDefectTreatment',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/safty-check/sc-team',
  //       name: '/safty-check/sc-team',
  //       component: () => import('@/views/safty-check/ScTeam/ScTeam.vue'),
  //       meta: {
  //         title: '安检班组',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/safty-check/sc-team',
  //         code: 'SaftyCheckTeam',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //   ],
  // },
  // {
  //   path: '/task',
  //   name: '/task',
  //   component: getLayoutComponent(),
  //   meta: {
  //     title: '工单管理',
  //     icon: 'Antd-CarryOutTwotone',
  //     noCache: false,
  //     type: 'dirt',
  //     href: '/task',
  //     code: 'Layout',
  //   },
  //   hideChildrenInMenu: true,
  //   children: [
  //     {
  //       path: '/work-order/type',
  //       name: '/work-order/type',
  //       component: () => import('@/views/work-order/WorkOrderType/WorkOrderType.vue'),
  //       meta: {
  //         title: '工单类型',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/work-order/type',
  //         code: 'WorkOrderType',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/org/outworker',
  //       name: '/org/outworker',
  //       component: () => import('@/views/work-order/OwTeam/OwTeam.vue'),
  //       meta: {
  //         title: '外勤班组',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/org/outworker',
  //         code: 'OutWorkerTeam',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/work-order/apply',
  //       name: '/work-order/apply',
  //       component: () => import('@/views/work-order/WorkOrderApply/WorkOrderApply.vue'),
  //       meta: {
  //         title: '工单受理',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/work-order/apply',
  //         code: 'WorkOrderApply',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/work-order/map-dispatch',
  //       name: '/work-order/map-dispatch',
  //       component: () => import('@/views/work-order/WorkOrderMapDispatch/WorkOrderMapDispatch.vue'),
  //       meta: {
  //         title: '地图派工',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/work-order/map-dispatch',
  //         code: 'WorkOrderMapDispatch',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/work-order/dispatch',
  //       name: '/work-order/dispatch',
  //       component: () => import('@/views/work-order/WorkOrderDispatch/WorkOrderDispatch.vue'),
  //       meta: {
  //         title: '工单派工',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/work-order/dispatch',
  //         code: 'WorkOrderDispatch',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/work-order/reassignment',
  //       name: '/work-order/reassignment',
  //       component: () =>
  //         import('@/views/work-order/WorkOrderReassignment/WorkOrderReassignment.vue'),
  //       meta: {
  //         title: '工单改派',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/work-order/reassignment',
  //         code: 'WorkOrderReassignment',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/work-order/handle',
  //       name: '/work-order/handle',
  //       component: () => import('@/views/work-order/WorkOrderHandle/WorkOrderHandle.vue'),
  //       meta: {
  //         title: '工单处理',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/work-order/handle',
  //         code: 'WorkOrderHandle',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/work-order/center',
  //       name: '/work-order/center',
  //       component: () => import('@/views/work-order/WorkOrderCenter/WorkOrderCenter.vue'),
  //       meta: {
  //         title: '工单中心',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/work-order/center',
  //         code: 'WorkOrderCenter',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //   ],
  // },

  {
    path: "/org",
    name: "/org",
    component: getLayoutComponent(),
    meta: {
      title: "组织管理",
      icon: "Antd-GlobalOutlined",
      noCache: false,
      type: "dirt",
      href: "/org",
      code: "Layout",
    },
    hideChildrenInMenu: true,
    children: [
      {
        path: "/org/corp",
        name: "/org/corp",
        component: () =>
          import(
            "@/views/customer-service/org/StationCompany/StationCompany.vue"
          ),
        meta: {
          title: "公司管理",
          noCache: false,
          type: "menu",
          href: "/org/corp",
          code: "OrgCorp",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/org/mgt",
        name: "/org/mgt",
        component: () =>
          import("@/views/customer-service/org/StationInfo/StationInfo.vue"),
        meta: {
          title: "管理站信息",
          noCache: false,
          type: "menu",
          href: "/org/mgt",
          code: "OrgMgt",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/org/user",
        name: "/org/user",
        component: () =>
          import("@/views/customer-service/org/BusUser/BusUser.vue"),
        meta: {
          title: "人员管理",
          noCache: false,
          type: "menu",
          href: "/org/user",
          code: "OrgUser",
        },
        hideChildrenInMenu: true,
      },
    ],
  },

  {
    path: "/iot4",
    name: "/iot4",
    component: getLayoutComponent(),
    meta: {
      title: "远传采集",
      icon: "Antd-DatabaseTwotone",
      noCache: false,
      type: "dirt",
      href: "/iot4",
      code: "Layout",
    },
    hideChildrenInMenu: true,
    children: [
      {
        path: "/iot/iot-devInfo",
        name: "/iot/iot-devInfo",
        component: () => import("@/views/iot/IotDevInfo/IotDevInfo.vue"),
        meta: {
          title: "远传设备",
          noCache: false,
          type: "menu",
          href: "/iot/iot-devInfo",
          code: "IotDevInfo",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/iot/collection-records",
        name: "/iot/collection-records",
        component: () =>
          import("@/views/iot/IotCollectionRecords/IotCollectionRecords.vue"),
        meta: {
          title: "采集记录",
          noCache: false,
          type: "menu",
          href: "/iot/collection-records",
          code: "IOTCollectionRecords",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/iot/alarm-records",
        name: "/iot/alarm-records",
        component: () =>
          import("@/views/iot/MeterAlarmInfo/MeterAlarmInfo.vue"),
        meta: {
          title: "报警记录",
          noCache: false,
          type: "menu",
          href: "/iot/alarm-records",
          code: "IOTAlarmRecords",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/iot/meter-monitor",
        name: "/iot/meter-monitor",
        component: () =>
          import("@/views/iot/IotMeterMonitor/IotMeterMonitor.vue"),
        meta: {
          title: "采集监控",
          noCache: false,
          type: "menu",
          href: "/iot/meter-monitor",
          code: "IOTMeterMonitor",
        },
        hideChildrenInMenu: true,
      },
    ],
  },
  {
    path: "/iot3",
    name: "/iot3",
    component: getLayoutComponent(),
    meta: {
      title: "远程控制",
      icon: "Antd-InteractionTwotone",
      noCache: false,
      type: "dirt",
      href: "/iot3",
      code: "Layout",
    },
    hideChildrenInMenu: true,
    children: [
      // {
      //   path: '/iot/sys-conf',
      //   name: '/iot/sys-conf',
      //   component: () => import('@/views/iot/IotSysConf/IotSysConf.vue'),
      //   meta: {
      //     title: '系统参数',
      //     noCache: false,
      //     type: 'menu',
      //     href: '/iot/sys-conf',
      //     code: 'IOTSysConf',
      //   },
      //   hideChildrenInMenu: true,
      // },
      {
        path: "/iot/agreement",
        name: "/iot/agreement",
        component: () => import("@/views/iot/IotAgreement/IotAgreement.vue"),
        meta: {
          title: "协议查询",
          noCache: false,
          type: "menu",
          href: "/iot/agreement",
          code: "IOTAgreement",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/iot/remote-control",
        name: "/iot/remote-control",
        component: () =>
          import("@/views/iot/IotRemoteControl/IotRemoteControl.vue"),
        meta: {
          title: "远传控制",
          noCache: false,
          type: "menu",
          href: "/iot/remote-control",
          code: "IOTRemoteControl",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/iot/value-control",
        name: "/iot/value-control",
        component: () =>
          import("@/views/iot/IotValueControl/IotValueControl.vue"),
        meta: {
          title: "阀门控制",
          noCache: false,
          type: "menu",
          href: "/iot/value-control",
          code: "IOTValueControl",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/iot/control-records",
        name: "/iot/control-records",
        component: () =>
          import("@/views/iot/IotControlRecords/IotControlRecords.vue"),
        meta: {
          title: "控制记录",
          noCache: false,
          type: "menu",
          href: "/iot/control-records",
          code: "IOTControlRecords",
        },
        hideChildrenInMenu: true,
      },
    ],
  },

  {
    path: "/system",
    name: "/system",
    component: getLayoutComponent(),
    meta: {
      title: "系统管理",
      icon: "Antd-SettingTwotone",
      noCache: false,
      type: "dirt",
      href: "/system",
      code: "Layout",
    },
    hideChildrenInMenu: true,
    children: [
      {
        path: "/uias/redis",
        name: "/uias/redis",
        component: () => import("@/views/uias/RedisManage/RedisManage.vue"),
        meta: {
          title: "Redis维护",
          icon: "Antd-RedditOutlined",
          noCache: false,
          type: "menu",
          href: "/uias/redis",
          code: "RedisManage",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/system/printer-template",
        name: "/system/printer-template",
        component: () =>
          import("@/views/system/CustAccountTemp/CustAccountTemp.vue"),
        meta: {
          title: "打印模板",
          icon: "Antd-PrinterTwotone",
          noCache: false,
          type: "menu",
          href: "/printer/template",
          code: "CustAccountTemp",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/uias/sys-dic",
        name: "/uias/sys-dic",
        component: () => import("@/views/system/SysDic/SysDic.vue"),
        meta: {
          title: "字典维护",
          icon: "Antd-BookTwotone",
          noCache: false,
          type: "menu",
          href: "/system/sys-dic",
          code: "SysDic",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/uias/dictionary",
        name: "/uias/dictionary",
        component: () =>
          import("@/views/uias/BaseDictionary/BaseDictionary.vue"),
        meta: {
          title: "字典管理",
          icon: "Antd-BookTwotone",
          noCache: false,
          type: "menu",
          href: "/uias/dictionary",
          code: "UiasDictionary",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/uias/user",
        name: "/uias/user",
        component: () => import("@/views/uias/BaseUser/BaseUser.vue"),
        meta: {
          title: "用户管理",
          icon: "Antd-BookTwotone",
          noCache: false,
          type: "menu",
          href: "/uias/user",
          code: "UiasBaseUser",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/uias/org",
        name: "/uias/org",
        component: () => import("@/views/uias/Organization/Organization.vue"),
        meta: {
          title: "组织架构",
          icon: "Antd-ApartmentOutlined",
          noCache: false,
          type: "menu",
          href: "/uias/org",
          code: "UiasOrg",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/uias/user-group",
        name: "/uias/user-group",
        component: () => import("@/views/uias/UserGroup/UserGroup.vue"),
        meta: {
          title: "用户组管理",
          icon: "Antd-FolderTwotone",
          noCache: false,
          type: "menu",
          href: "/uias/user-group",
          code: "UiasUserGroup",
        },
        hideChildrenInMenu: true,
      },

      // {
      //   path: '/system/app',
      //   name: '/system/app',
      //   component: () => import('@/views/system/BusAppVersion/BusAppVersion.vue'),
      //   meta: {
      //     title: '移动应用',
      //     icon: 'Antd-AndroidFilled',
      //     noCache: false,
      //     type: 'menu',
      //     href: '/system/app',
      //     code: 'SystemApp',
      //   },
      //   hideChildrenInMenu: true,
      // },
      // {
      //   path: '/system/notice',
      //   name: '/system/notice',
      //   component: () => import('@/views/system/SystemNotice/SystemNotice.vue'),
      //   meta: {
      //     title: '系统公告',
      //     icon: 'Antd-BellTwotone',
      //     noCache: false,
      //     type: 'menu',
      //     href: '/system/notice',
      //     code: 'SystemNotice',
      //   },
      //   hideChildrenInMenu: true,
      // },
      // {
      //   path: '/system/news',
      //   name: '/system/news',
      //   component: () => import('@/views/system/SystemNews/SystemNews.vue'),
      //   meta: {
      //     title: '站内新闻',
      //     icon: 'Antd-PictureTwotone',
      //     noCache: false,
      //     type: 'menu',
      //     href: '/system/news',
      //     code: 'SystemNews',
      //   },
      //   hideChildrenInMenu: true,
      // },
      {
        path: "/system/config",
        name: "/system/config",
        component: () => import("@/views/system/SystemConfig/SystemConfig.vue"),
        meta: {
          title: "系统参数",
          noCache: false,
          icon: "Antd-ControlTwotone",
          type: "menu",
          href: "/system/config",
          code: "SystemConfig",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/system/log",
        name: "/system/log",
        component: () => import("@/views/system/SysOperLog/SysOperLog.vue"),
        meta: {
          title: "审计日志",
          noCache: false,
          type: "menu",
          icon: "Antd-BugTwotone",
          href: "/system/log",
          code: "SysOperLog",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/system/msg-log",
        name: "/system/msg-log",
        component: () => import("@/views/system/SysMsgLog/SysMsgLog.vue"),
        meta: {
          title: "短信日志",
          noCache: false,
          icon: "Antd-CloudTwotone",
          type: "menu",
          href: "/system/msg-log",
          code: "SysMsgLog",
        },
        hideChildrenInMenu: true,
      },
      // {
      //   path: '/system/feedback',
      //   name: '/system/feedback',
      //   meta: {
      //     title: '意见反馈',
      //     icon: 'Antd-BugTwotone',
      //     noCache: false,
      //     type: 'menu',
      //     href: '/system/feedback',
      //     code: 'SystemFeedback',
      //   },
      //   hideChildrenInMenu: true,
      // },

      // {
      //   path: '/system/material-maintenance',
      //   name: '/system/material-maintenance',
      //   component: () => import('@/views/system/WoCostType/WoCostType.vue'),
      //   meta: {
      //     title: '材料维护',
      //     icon: 'Antd-ApiTwotone',
      //     noCache: false,
      //     type: 'menu',
      //     href: '/system/material-maintenance',
      //     code: 'MaterialMaintenance',
      //   },
      //   hideChildrenInMenu: true,
      // },
      {
        path: "/system/self-maintain",
        name: "/system/self-maintain",
        component: () => import("@/views/system/SelfMaintain/SelfMaintain.vue"),
        meta: {
          title: "自助设备",
          icon: "Antd-DatabaseTwotone",
          noCache: false,
          type: "menu",
          href: "/system/self-maintain",
          code: "SelfMaintain",
        },
        hideChildrenInMenu: true,
      },
    ],
  },
  // {
  //   path: '/workflow',
  //   name: '/workflow',
  //   component: getLayoutComponent(),
  //   meta: {
  //     title: '流程管理',
  //     icon: 'Antd-BranchesOutlined',
  //     noCache: false,
  //     type: 'dirt',
  //     href: '/workflow',
  //     code: 'Layout',
  //   },
  //   hideChildrenInMenu: true,
  //   children: [
  //     {
  //       path: '/workflow/config',
  //       name: '/workflow/config',
  //       component: () => import('@/views/workflow/WorkflowConfig.vue'),
  //       meta: {
  //         title: '流程配置',
  //         icon: 'Antd-ToolTwotone',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/workflow/config',
  //         code: 'WorkflowConfig',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/workflow/condition',
  //       name: '/workflow/condition',
  //       component: () => import('@/views/workflow/WorkFlowCondition.vue'),
  //       meta: {
  //         title: '条件定义',
  //         icon: 'Antd-CalculatorTwotone',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/workflow/condition',
  //         code: 'WorkflowCondition',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/workflow/manage',
  //       name: '/workflow/manage',
  //       component: () => import('@/views/workflow/WorkFlowManage.vue'),
  //       meta: {
  //         title: '流程管理',
  //         icon: 'Antd-HddTwotone',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/workflow/manage',
  //         code: 'WorkflowManage',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/workflow/reassign',
  //       name: '/workflow/reassign',
  //       component: () => import('@/views/workflow/WorkFlowReassignment.vue'),
  //       meta: {
  //         title: '流程改派',
  //         icon: 'Antd-DeploymentUnitOutlined',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/workflow/reassign',
  //         code: 'WorkflowReassign',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //   ],
  // },
  {
    path: "/task-schedule",
    name: "/task-schedule",
    component: getLayoutComponent(),
    meta: {
      title: "任务调度",
      icon: "Antd-LockTwotone",
      noCache: false,
      type: "dirt",
      href: "/task-schedule",
      code: "Layout",
    },
    hideChildrenInMenu: true,
    children: [
      {
        path: "/task-schedule/exector",
        name: "/task-schedule/exector",
        component: () => import("@/views/uias/TaskExector/TaskExector.vue"),
        meta: {
          title: "执行器",
          noCache: false,
          type: "menu",
          href: "/task-schedule/exector",
          code: "TaskExector",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/task-schedule/task-info",
        name: "/task-schedule/task-info",
        component: () => import("@/views/uias/TaskInfo/TaskInfo.vue"),
        meta: {
          title: "任务管理",
          noCache: false,
          type: "menu",
          href: "/task-schedule/task-info",
          code: "TaskInfo",
        },
        hideChildrenInMenu: true,
      },
      {
        path: "/task-schedule/log",
        name: "/task-schedule/log",
        component: () => import("@/views/uias/TaskLog/TaskLog.vue"),
        meta: {
          title: "调度日志",
          noCache: false,
          type: "menu",
          href: "/task-schedule/log",
          code: "TaskLog",
        },
        hideChildrenInMenu: true,
      },
    ],
  },
  // {
  //   path: '/iot',
  //   name: '/iot',
  //   component: getLayoutComponent(),
  //   meta: {
  //     title: '采集监控',
  //     icon: 'Antd-DashboardTwotone',
  //     noCache: false,
  //     type: 'dirt',
  //     href: '/iot',
  //     code: 'Layout',
  //   },
  //   hideChildrenInMenu: true,
  //   children: [
  //     {
  //       path: '/iot/dashboard',
  //       name: '/iot/dashboard',
  //       component: () => import('@/views/iot/IotDashBoard/IotDashBoard.vue'),
  //       meta: {
  //         title: 'IOT仪表盘',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/iot/dashboard',
  //         code: 'IOTDashBoard',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: 'http://gcis-in.izoyo.net/fi-iot-data-v',
  //       name: 'http://gcis-in.izoyo.net/fi-iot-data-v',
  //       meta: {
  //         title: '运行监控',
  //         noCache: false,
  //         type: 'menu',
  //         href: 'http://gcis-in.izoyo.net/fi-iot-data-v',
  //         code: 'Normal',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: 'http://gcis-in.izoyo.net/fi-iot-data-v?p=2',
  //       name: 'http://gcis-in.izoyo.net/fi-iot-data-v?p=2',
  //       meta: {
  //         title: '网络监控',
  //         noCache: false,
  //         type: 'menu',
  //         href: 'http://gcis-in.izoyo.net/fi-iot-data-v?p=2',
  //         code: 'Normal',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //   ],
  // },

  // {
  //   path: '/report',
  //   name: '/report',
  //   component: getLayoutComponent(),
  //   meta: {
  //     title: '客服报表',
  //     icon: 'Antd-CodeSandboxOutlined',
  //     noCache: false,
  //     type: 'dirt',
  //     href: '/report',
  //     code: 'Layout',
  //   },
  //   hideChildrenInMenu: true,
  //   children: [
  //     {
  //       path: '/report/customer-develop',
  //       name: '/report/customer-develop',
  //       component: () => import('@/views/report/customer-development.vue'),
  //       meta: {
  //         title: '客户发展',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/report/customer-develop',
  //         code: 'CustomerDevelop',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/report/meter-num',
  //       name: '/report/meter-num',
  //       component: () => import('@/views/report/meter-num.vue'),
  //       meta: {
  //         title: '表具数量',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/report/meter-num',
  //         code: 'MeterNum',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/report/achievements-staff',
  //       name: '/report/achievements-staff',
  //       component: () => import('@/views/report/achievements-staff.vue'),
  //       meta: {
  //         title: '业务办理量(员工)',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/report/achievements-staff',
  //         code: 'AchievementsStaff',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/report/achievements-customer',
  //       name: '/report/achievements-customer',
  //       component: () => import('@/views/report/achievements-customer.vue'),
  //       meta: {
  //         title: '业务办理量(客户)',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/report/achievements-customer',
  //         code: 'AchievementsCustomer',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/report/meter-reading-statistics ',
  //       name: '/report/meter-reading-statistics ',
  //       component: () => import('@/views/report/meter-reading-statistics.vue'),
  //       meta: {
  //         title: '抄表情况统计表',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/report/meter-reading-statistics ',
  //         code: 'MeterReadingStatistics',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/report/statistics-of-meter-reading-customer-type',
  //       name: '/report/statistics-of-meter-reading-customer-type',
  //       component: () => import('@/views/report/statistics-of-meter-reading-customer-type.vue'),
  //       meta: {
  //         title: '抄表情况统计(客户类型)',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/report/statistics-of-meter-reading-customer-type',
  //         code: 'StatisticsOfMeterReadingCustomerType',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/report/gas-consumption-statistics',
  //       name: '/report/gas-consumption-statistics',
  //       component: () => import('@/views/report/gas-consumption-statistics.vue'),
  //       meta: {
  //         title: '用量统计表',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/report/gas-consumption-statistics',
  //         code: 'GasConsumptionStatistics',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/report/gas-charge-recovery-rate',
  //       name: '/report/gas-charge-recovery-rate',
  //       component: () => import('@/views/report/gas-charge-recovery-rate.vue'),
  //       meta: {
  //         title: '气费回收率',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/report/gas-charge-recovery-rate',
  //         code: 'GasChargeRecoveryRate',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/report/meter-reading-long-time-not-read-user',
  //       name: '/report/meter-reading-long-time-not-read-user',
  //       component: () => import('@/views/report/meter-reading-long-time-not-read-user.vue'),
  //       meta: {
  //         title: '长期未抄用户明细(抄表)',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/report/meter-reading-long-time-not-read-user',
  //         code: 'MeterReadingLongTimeNotReadUser',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/report/meter-reading-long-time-not-use-user',
  //       name: '/report/meter-reading-long-time-not-use-user',
  //       component: () => import('@/views/report/meter-reading-long-time-not-use-user.vue'),
  //       meta: {
  //         title: '长期未用用户明细(抄表)',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/report/meter-reading-long-time-not-use-user',
  //         code: 'MeterReadingLongTimeNotUseUser',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/report/abnormal-meter-reading-account',
  //       name: '/report/abnormal-meter-reading-account',
  //       component: () => import('@/views/report/abnormal-meter-reading-account.vue'),
  //       meta: {
  //         title: '抄表异常户明细',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/report/abnormal-meter-reading-account',
  //         code: 'AbnormalMeterReadingAccount',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //   ],
  // },
  // {
  //   path: '/report-safty-checking',
  //   name: '/report-safty-checking',
  //   component: getLayoutComponent(),
  //   meta: {
  //     title: '安检报表',
  //     icon: 'Antd-CodeSandboxOutlined',
  //     noCache: false,
  //     type: 'dirt',
  //     href: '/report-safty-checking',
  //     code: 'Layout',
  //   },
  //   hideChildrenInMenu: true,
  //   children: [
  //     {
  //       path: '/report/annual-security-inspection-plan-statistics',
  //       name: '/report/annual-security-inspection-plan-statistics',
  //       component: () => import('@/views/report/annual-security-inspection-plan-statistics.vue'),
  //       meta: {
  //         title: '年度安检计划完成统计',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/report/annual-security-inspection-plan-statistics',
  //         code: 'AnnualSecurityInspectionPlanStatistics',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/report/security-risks-user-type',
  //       name: '/report/security-risks-user-type',
  //       component: () => import('@/views/report/security-risks-user-type.vue'),
  //       meta: {
  //         title: '安检隐患及整改情况统计-用户类型',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/report/security-risks-user-type',
  //         code: 'SecurityRisksUserType',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/report/month-safty-check-complete',
  //       name: '/report/month-safty-check-complete',
  //       component: () => import('@/views/report/month-safty-check-complete.vue'),
  //       meta: {
  //         title: '月度安检计划完成统计',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/report/month-safty-check-complete',
  //         code: 'MonthSaftyCheckComplete',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/report/workload-statistics-of-personnel-security-inspection',
  //       name: '/report/workload-statistics-of-personnel-security-inspection',
  //       component: () =>
  //         import('@/views/report/workload-statistics-of-personnel-security-inspection.vue'),
  //       meta: {
  //         title: '人员安检工作量统计',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/report/workload-statistics-of-personnel-security-inspection',
  //         code: 'WorkloadStatisticsOfPersonnelSecurityInspection',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/report/completion-rate-of-community-security-inspection',
  //       name: '/report/completion-rate-of-community-security-inspection',
  //       component: () =>
  //         import('@/views/report/completion-rate-of-community-security-inspection.vue'),
  //       meta: {
  //         title: '小区安检完成率统计',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/report/completion-rate-of-community-security-inspection',
  //         code: 'CompletionRateOfCommunitySecurityInspection',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/report/security-dangers-inspection-and-rectification',
  //       name: '/report/security-dangers-inspection-and-rectification',
  //       component: () => import('@/views/report/security-dangers-inspection-and-rectification.vue'),
  //       meta: {
  //         title: '安检隐患及整改情况统计',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/report/security-dangers-inspection-and-rectification',
  //         code: 'SecurityDangersInspectionAndRectification',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/report/indoor-gas-appliances-statistics',
  //       name: '/report/indoor-gas-appliances-statistics',
  //       component: () => import('@/views/report/indoor-gas-appliances-statistics.vue'),
  //       meta: {
  //         title: '户内燃具统计',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/report/indoor-gas-appliances-statistics',
  //         code: 'IndoorGasAppliancesStatistics',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //   ],
  // },
  // {
  //   path: '/report-work-order',
  //   name: '/report-work-order',
  //   component: getLayoutComponent(),
  //   meta: {
  //     title: '工单报表',
  //     icon: 'Antd-CodeSandboxOutlined',
  //     noCache: false,
  //     type: 'dirt',
  //     href: '/report-work-order',
  //     code: 'Layout',
  //   },
  //   hideChildrenInMenu: true,
  //   children: [
  //     {
  //       path: '/report/work-order-acceptance',
  //       name: '/report/work-order-acceptance',
  //       component: () => import('@/views/report/work-order-acceptance.vue'),
  //       meta: {
  //         title: '工单受理情况',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/report/work-order-acceptance',
  //         code: 'WorkOrderAcceptance',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/report/work-order-statement',
  //       name: '/report/work-order-statement',
  //       component: () => import('@/views/report/work-order-statement.vue'),
  //       meta: {
  //         title: '工单结单情况',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/report/work-order-statement',
  //         code: 'WorkOrderStatement',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/reprot/work-order-classification-indicator-statistics',
  //       name: '/reprot/work-order-classification-indicator-statistics',
  //       component: () =>
  //         import('@/views/report/work-order-classification-indicator-statistics.vue'),
  //       meta: {
  //         title: '工单分类指标统计表',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/reprot/work-order-classification-indicator-statistics',
  //         code: 'WorkOrderClassificationIndicatorStatistics',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/report/work-order-details',
  //       name: '/report/work-order-details',
  //       component: () => import('@/views/report/work-order-details.vue'),
  //       meta: {
  //         title: '工单明细',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/report/work-order-details',
  //         code: 'WorkOrderDetails',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/report/work-order-material-cost-statistics',
  //       name: '/report/work-order-material-cost-statistics',
  //       component: () => import('@/views/report/work-order-material-cost-statistics.vue'),
  //       meta: {
  //         title: '工单材料费用统计表',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/report/work-order-material-cost-statistics',
  //         code: 'WorkOrderMaterialCostStatistics',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //     {
  //       path: '/report/work-order-material-cost-detail-statistics',
  //       name: '/report/work-order-material-cost-detail-statistics',
  //       component: () => import('@/views/report/work-order-material-cost-detail-statistics.vue'),
  //       meta: {
  //         title: '工单材料费用明细统计表',
  //         noCache: false,
  //         type: 'menu',
  //         href: '/report/work-order-material-cost-detail-statistics',
  //         code: 'WorkOrderMaterialCostDetailStatistics',
  //       },
  //       hideChildrenInMenu: true,
  //     },
  //   ],
  // },
];
export default route;
