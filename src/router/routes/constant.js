import LayoutDefault from "../../layout/default/index.vue";
import FIDefault from "../../layout/fi/index.vue";

const env = "test";
const getLayoutComponent = () => {
  if (env === "preview") {
    return FIDefault;
  }
  return LayoutDefault;
};
const route = [
  {
    path: "/",
    component: getLayoutComponent(),
    alwaysShow: false,
    redirect: "/dashboard",
    meta: {
      title: "仪表盘",
    },
    children: [
      {
        name: "Dashboard",
        path: "/dashboard",
        component: () => import("@/views/sys/dashboard.vue"),
        meta: {
          title: "仪表盘",
          icon: "Antd-DashboardOutlined",
          breadcrumb: false,
        },
      },
    ],
  },
  {
    name: "Login",
    path: "/Login",
    component: () => import("@/views/sys/auth/login.vue"),
    hidden: true,
  },
  {
    name: "Register",
    path: "/register",
    component: () => import("@/views/sys/auth/register.vue"),
    hidden: true,
  },
  {
    name: "Reset",
    path: "/reset",
    component: () => import("@/views/sys/auth/reset.vue"),
    hidden: true,
  },
];
export default route;
